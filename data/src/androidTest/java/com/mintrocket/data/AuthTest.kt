package com.mintrocket.data

import android.support.test.runner.AndroidJUnit4
import com.mintrocket.data.api.request.auth.SignInRequest
import com.mintrocket.data.api.response.auth.SignInResponse
import com.mintrocket.data.api.service.AuthService
import com.mintrocket.data.di.dataModule
import com.mintrocket.data.repository.AuthRepository
import com.mintrocket.data.storage.IAuthStorage
import com.mintrocket.domain.domainModule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.standalone.KoinComponent
import org.koin.standalone.StandAloneContext.startKoin
import org.koin.standalone.get

@RunWith(AndroidJUnit4::class)
class AuthTest : KoinComponent {


    @Test
    fun auth() {
        startKoin(listOf(dataModule))
        val authService = get<AuthService>()

        val req = SignInRequest("Daniil", "Qwe123")


        authService.signInByUsername(req)
            .subscribe({
                assert(true)
            }, {
                assert(false)
            })
    }
}