package com.mintrocket.data.di

import com.google.gson.GsonBuilder
import com.mintrocket.data.BuildConfig
import com.mintrocket.data.DataErrorHandler
import com.mintrocket.data.api.DateAdapter
import com.mintrocket.data.api.interceptor.AuthInterceptor
import com.mintrocket.data.api.service.AuthService
import com.mintrocket.data.api.service.ManagementCompanyService
import com.mintrocket.data.api.service.NewsService
import com.mintrocket.data.repository.AuthRepository
import com.mintrocket.data.repository.ManagementCompanyRepository
import com.mintrocket.data.repository.NewsRepository
import com.mintrocket.data.repository.SettingsRepository
import com.mintrocket.data.storage.AppPreferences
import com.mintrocket.data.storage.IAppStorage
import com.mintrocket.data.storage.IAuthStorage
import com.mintrocket.domain.IDataErrorHandler
import com.mintrocket.domain.repository.IAuthRepository
import com.mintrocket.domain.repository.IManagementCompanyRepository
import com.mintrocket.domain.repository.INewsRepository
import com.mintrocket.domain.repository.ISettingsRepository
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.Module
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.*

val dataModule: Module = module {

    //    val sharedPreferences = AppPreferences(androidApplication())
    factory<IAuthStorage> { AppPreferences(androidApplication()) }
    factory<IAppStorage> { AppPreferences(androidApplication()) }

    factory {
        OkHttpClient.Builder()
            .addInterceptor(AuthInterceptor(get()))
            .addNetworkInterceptor(HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { Timber.tag("logging").d(it) })
                .setLevel(if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE))
            .addInterceptor { chain ->
                val newRequest = chain.request()
                    .newBuilder()
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Accept", "application/json")
                    .build()
                chain.proceed(newRequest)
            }
            .build()
    }

    factory {
        GsonBuilder()
            .registerTypeAdapter(Date::class.java, DateAdapter())
            .create()
    }

    factory {
        Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(get()))
            .client(get())
            .baseUrl("http://api.hightechdom.mintrocket.ru/api/v1/")
            .build()
    }

    factory { DataErrorHandler(get(), get()) as IDataErrorHandler }

    factory { get<Retrofit>().create(AuthService::class.java) }
    factory { get<Retrofit>().create(NewsService::class.java) }
    factory { get<Retrofit>().create(ManagementCompanyService::class.java) }

    factory<IAuthRepository> { AuthRepository(get(), get()) }
    factory<INewsRepository> { NewsRepository(get()) }
    factory<IManagementCompanyRepository> { ManagementCompanyRepository(get()) }
    factory<ISettingsRepository> { SettingsRepository(get()) }
}