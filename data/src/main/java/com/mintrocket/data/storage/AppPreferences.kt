package com.mintrocket.data.storage

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

class AppPreferences(
    private val context: Context
) : IAuthStorage, IAppStorage {

    private val sp: SharedPreferences
        get() = PreferenceManager.getDefaultSharedPreferences(context)

    override var accessToken: String
        get() = sp.getString("access_token", "") ?: ""
        set(value) {
            sp.edit().putString("access_token", value).apply()
        }

    override var refreshToken: String
        get() = sp.getString("refresh_token", "") ?: ""
        set(value) {
            sp.edit().putString("refresh_token", "").apply()
        }

    override var tokenType: String
        get() = sp.getString("token_type", "") ?: ""
        set(value) {
            sp.edit().putString("token_type", value).apply()
        }

    override var expiresIn: Long
        get() = sp.getLong("expires_in", 0L)
        set(value) {
            sp.edit().putLong("expires_in", value).apply()
        }

    override var isRegistered: Boolean
        get() = sp.getBoolean("is_registered", false)
        set(value) {
            sp.edit().putBoolean("is_registered", value).apply()
        }

    override var isDarkTheme: Boolean
        get() = sp.getBoolean("is_dark_theme", false)
        set(value) {
            sp.edit().putBoolean("is_dark_theme", value).apply()
        }
}