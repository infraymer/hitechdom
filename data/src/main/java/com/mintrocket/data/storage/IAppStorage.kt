package com.mintrocket.data.storage

interface IAppStorage {

    var isDarkTheme: Boolean
}