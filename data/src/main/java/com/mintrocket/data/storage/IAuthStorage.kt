package com.mintrocket.data.storage

interface IAuthStorage {

    var tokenType: String
    var expiresIn: Long
    var accessToken: String
    var refreshToken: String

    var isRegistered: Boolean
}