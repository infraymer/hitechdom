package com.mintrocket.data.api.response.common

import com.google.gson.annotations.SerializedName
import com.mintrocket.data.api.converter.toDomain
import com.mintrocket.domain.entity.Entity
import io.reactivex.Single
import io.reactivex.SingleTransformer

class BaseResponse<T>(
    @JvmField
    @SerializedName("status")
    val status: Boolean,

    @JvmField
    @SerializedName("data")
    val data: T?,

    @JvmField
    @SerializedName("errors")
    val errors: List<ErrorResponse>?
) {

    fun handleError(): Single<BaseResponse<T>> = when {
        status -> Single.just(this)
        errors != null -> Single.error(errors.toDomain())
        else -> Single.error(RuntimeException("Неизвестная ошибка"))
    }

    companion object {
        fun <T, V> fetchResult(converter: (T) -> V): SingleTransformer<BaseResponse<T>, V> = SingleTransformer {
            it.flatMap { it.handleError() }.map { converter(it.data!!) }
        }

        @JvmStatic
        fun <T> fetchResult(): SingleTransformer<BaseResponse<T>, T> = SingleTransformer {
            it.flatMap { it.handleError() }.map { it.data!! }
        }

        @JvmStatic
        fun <T> fetchResultNullable(): SingleTransformer<BaseResponse<T>, Entity<T>> = SingleTransformer {
            it.flatMap { it.handleError() }.map { Entity(it.data) }
        }
    }
}