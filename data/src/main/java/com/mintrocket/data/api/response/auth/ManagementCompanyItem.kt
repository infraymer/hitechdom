package com.mintrocket.data.api.response.auth

import com.google.gson.annotations.SerializedName

class ManagementCompanyItem(
    @SerializedName("id") val id: Long,
    @SerializedName("name") val name: String?,
    @SerializedName("email") val email: String?
)