package com.mintrocket.data.api.response.news

import com.google.gson.annotations.SerializedName

class NewsMeta(
    @SerializedName("current_page") val currentPage: Int,
    @SerializedName("last_page") val lastPage: Int,
    @SerializedName("per_page") val perPage: Int,
    @SerializedName("total") val total: Int
)