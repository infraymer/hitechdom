package com.mintrocket.data.api.request.news

import com.google.gson.annotations.SerializedName

class NewsRequest(
    @SerializedName("page") val page: Int,
    @SerializedName("archive") val archive: String? = null,
    @SerializedName("per_page") val perPage: Int? = null
)