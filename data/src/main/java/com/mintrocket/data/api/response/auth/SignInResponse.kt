package com.mintrocket.data.api.response.auth

import com.google.gson.annotations.SerializedName

class SignInResponse(
    @SerializedName("token_type") val tokenType: String,
    @SerializedName("expires_in") val expiresIn: Long,
    @SerializedName("access_token") val accessToken: String,
    @SerializedName("refresh_token") val refreshToken: String,
    @SerializedName("user") val user: UserResponse,
    @SerializedName("is_registered") val isRegistered: Boolean
)