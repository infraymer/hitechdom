package com.mintrocket.data.api.response.auth

import com.google.gson.annotations.SerializedName

class PolicyResponse(
    @SerializedName("name") val name: String?,
    @SerializedName("content") val content: String?
)