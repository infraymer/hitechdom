package com.mintrocket.data.api.request.managmentCompany

import com.google.gson.annotations.SerializedName

class FindByAddressRequest(
    @SerializedName("city") val city: String,
    @SerializedName("street") val street: String,
    @SerializedName("house") val house: String,
    @SerializedName("housing") val housing: String,
    @SerializedName("apartment") val apartment: String
)