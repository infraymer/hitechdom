package com.mintrocket.data.api.response.common

import com.google.gson.annotations.SerializedName
import com.mintrocket.data.api.converter.toDomain
import io.reactivex.Single
import io.reactivex.SingleTransformer

class EmptyResponse(
    @JvmField
    @SerializedName("status")
    val status: Boolean,

    @JvmField
    @SerializedName("errors")
    val errors: List<ErrorResponse>?
) {

    fun handleError(): Single<EmptyResponse> {
        return when {
            status -> Single.just(this)
            errors != null -> Single.error(errors.toDomain())
            else -> Single.error(RuntimeException("Неизвестная ошибка"))
        }
    }

    companion object {
        @JvmStatic
        fun fetchResult(): SingleTransformer<EmptyResponse, Unit> = SingleTransformer {
            it.flatMap { t -> t.handleError() }.map { Unit }
        }
    }

}