package com.mintrocket.data.api

import com.google.gson.*
import java.lang.reflect.Type
import java.util.*

class DateAdapter : JsonSerializer<Date>, JsonDeserializer<Date> {

    override fun serialize(src: Date, typeOfSrc: Type?, context: JsonSerializationContext?) = JsonPrimitive(src.time / 1000)

    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?) = Date(json.asLong * 1000)

}
