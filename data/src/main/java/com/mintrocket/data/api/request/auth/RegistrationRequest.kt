package com.mintrocket.data.api.request.auth

import com.google.gson.annotations.SerializedName

class RegistrationRequest(
    @SerializedName("firstname") val firstName: String,
    @SerializedName("patronymic") val secondName: String,
    @SerializedName("lastname") val lastName: String,
    @SerializedName("city") val city: String,
    @SerializedName("street") val street: String,
    @SerializedName("house") val house: String,
    @SerializedName("housing") val housing: String,
    @SerializedName("apartment") val flat: String
)