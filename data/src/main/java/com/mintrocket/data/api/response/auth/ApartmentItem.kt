package com.mintrocket.data.api.response.auth

import com.google.gson.annotations.SerializedName

class ApartmentItem(
    @SerializedName("id") val id: Long,
    @SerializedName("number") val number: String?,
    @SerializedName("created_at") val createdAt: Long?,
    @SerializedName("entrance") val entrance: EntranceItem?
)