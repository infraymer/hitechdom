package com.mintrocket.data.api.converter

import com.mintrocket.data.api.response.news.NewsItem
import com.mintrocket.domain.entity.news.News

fun NewsItem.toDomain() = News(
    id, name, content, author, publishedAt
)