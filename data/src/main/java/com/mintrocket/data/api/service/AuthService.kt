package com.mintrocket.data.api.service

import com.mintrocket.data.api.request.auth.*
import com.mintrocket.data.api.response.auth.PolicyResponse
import com.mintrocket.data.api.response.auth.SignInResponse
import com.mintrocket.data.api.response.auth.UserResponse
import com.mintrocket.data.api.response.common.BaseResponse
import com.mintrocket.data.api.response.common.EmptyResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT

interface AuthService {

    @POST("signinByUsername")
    fun signInByUsername(@Body request: SignInRequest): Single<BaseResponse<SignInResponse>>

    @POST("sendSms")
    fun sendSms(@Body request: SendSmsRequest): Single<EmptyResponse>

    @POST("signinBySmsCode")
    fun signInBySmsCode(@Body request: SignInSmsRequest): Single<BaseResponse<SignInResponse>>

    @GET("users/me")
    fun getUserProfile(): Single<BaseResponse<UserResponse>>

    @PUT("users/update")
    fun updateUserProfile(@Body request: UpdateUserRequest): Single<BaseResponse<UserResponse>>

    @POST("users/register")
    fun registration(@Body request: RegistrationRequest): Single<EmptyResponse>

    @GET("policies/show?id=1")
    fun getPolicy(): Single<BaseResponse<PolicyResponse>>
}