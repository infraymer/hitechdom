package com.mintrocket.data.api.request.auth

import com.google.gson.annotations.SerializedName

class UpdateUserRequest(
    @SerializedName("firstname") val firstName: String,
    @SerializedName("lastname") val lastName: String,
    @SerializedName("patronymic") val secondName: String
)