package com.mintrocket.data.api.response.auth

import com.google.gson.annotations.SerializedName

class HouseItem(
    @SerializedName("id") val id: Long,
    @SerializedName("street") val street: String?,
    @SerializedName("house") val house: String?,
    @SerializedName("housing") val housing: String?,
    @SerializedName("created_at") val createdAt: Long?,
    @SerializedName("updated_at") val updatedAt: Long?,
    @SerializedName("management_company") val managementCompany: ManagementCompanyItem?
)