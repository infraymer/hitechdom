package com.mintrocket.data.api.converter

import com.mintrocket.data.api.response.auth.*
import com.mintrocket.domain.entity.auth.Apartment
import com.mintrocket.domain.entity.auth.Entrance
import com.mintrocket.domain.entity.auth.House
import com.mintrocket.domain.entity.auth.User
import com.mintrocket.domain.entity.management_company.ManagementCompany

fun UserResponse.toDomain() = User(
    id,
    name,
    username,
    firstName,
    secondName,
    lastName,
    email,
    phone,
    roleId,
    managementCompanyId,
    createdAt,
    updatedAt,
    apartments?.map { it.toDomain() }
)

fun ApartmentItem.toDomain() = Apartment(
    id, number, createdAt, entrance?.toDomain()
)

fun EntranceItem.toDomain() = Entrance(
    id, number, storeys, createdAt, updatedAt, house?.toDomain()
)

fun HouseItem.toDomain() = House(
    id, street, house, housing, createdAt, updatedAt, managementCompany?.toDomain()
)

fun ManagementCompanyItem.toDomain() = ManagementCompany(
    id, name, email, null, null
)