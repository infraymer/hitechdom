package com.mintrocket.data.api.request.auth

import com.google.gson.annotations.SerializedName

class SendSmsRequest(
    @SerializedName("phone") val phone: String
)