package com.mintrocket.data.api.response.auth

import com.google.gson.annotations.SerializedName

class EntranceItem(
    @SerializedName("id") val id: Long,
    @SerializedName("number") val number: String?,
    @SerializedName("storeys") val storeys: Int?,
    @SerializedName("created_at") val createdAt: Long?,
    @SerializedName("updated_at") val updatedAt: Long?,
    @SerializedName("house") val house: HouseItem?
)