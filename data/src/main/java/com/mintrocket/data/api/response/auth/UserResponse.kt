package com.mintrocket.data.api.response.auth

import com.google.gson.annotations.SerializedName

class UserResponse(
    @SerializedName("id") val id: Long?,
    @SerializedName("name") val name: String?,
    @SerializedName("username") val username: String?,
    @SerializedName("firstname") val firstName: String?,
    @SerializedName("patronymic") val secondName: String?,
    @SerializedName("lastname") val lastName: String?,
    @SerializedName("email") val email: String?,
    @SerializedName("phone") val phone: String?,
    @SerializedName("role_id") val roleId: Int?,
    @SerializedName("management_company_id") val managementCompanyId: Int?,
    @SerializedName("created_at") val createdAt: Long?,
    @SerializedName("updated_at") val updatedAt: Long?,
    @SerializedName("apartments") val apartments: List<ApartmentItem>?
)