package com.mintrocket.data.api.converter

import com.mintrocket.data.api.response.auth.PolicyResponse
import com.mintrocket.data.api.response.auth.SignInResponse
import com.mintrocket.domain.entity.auth.Auth
import com.mintrocket.domain.entity.auth.Policy

fun SignInResponse.toDomain() = Auth(user.toDomain(), isRegistered)

fun PolicyResponse.toDomain() = Policy(name, content)