package com.mintrocket.data.api.response.news

import com.google.gson.annotations.SerializedName

class NewsResponse(
    @SerializedName("items") val items: List<NewsItem>,
    @SerializedName("meta") val meta: NewsMeta
)