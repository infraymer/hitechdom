package com.mintrocket.data.api.service

import com.mintrocket.data.api.request.news.NewsRequest
import com.mintrocket.data.api.response.common.BaseResponse
import com.mintrocket.data.api.response.news.NewsItem
import com.mintrocket.data.api.response.news.NewsResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface NewsService {

    @POST("news/index")
    fun getNews(@Body request: NewsRequest): Single<BaseResponse<NewsResponse>>

    @GET("news/show")
    fun getDetailNews(@Query("id") newsId: Long): Single<BaseResponse<NewsItem>>
}