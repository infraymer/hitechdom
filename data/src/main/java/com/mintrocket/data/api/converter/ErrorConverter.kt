package com.mintrocket.data.api.converter

import com.mintrocket.data.api.response.common.ErrorResponse
import com.mintrocket.domain.DataException
import com.mintrocket.domain.DataError

fun List<ErrorResponse>.toDomain() = DataException(this.map { DataError(it.code, it.message, it.description) })