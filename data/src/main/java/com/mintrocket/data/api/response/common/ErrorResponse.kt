package com.mintrocket.data.api.response.common

import com.google.gson.annotations.SerializedName

class ErrorResponse(
        @SerializedName("message")
        private val text: String,

        @JvmField
        @SerializedName("code")
        val code: String,

        @JvmField
        @SerializedName("description")
        val description: String
) : RuntimeException(text)