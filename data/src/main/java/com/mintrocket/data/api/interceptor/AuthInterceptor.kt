package com.mintrocket.data.api.interceptor

import com.mintrocket.data.storage.IAuthStorage
import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor(
    private val authStorage: IAuthStorage
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val token = authStorage.accessToken

        return chain.proceed(if (token.isNotEmpty()) {
            chain.request().newBuilder()
                .addHeader("Authorization", "Bearer $token")
                .build()
        } else chain.request())
    }
}