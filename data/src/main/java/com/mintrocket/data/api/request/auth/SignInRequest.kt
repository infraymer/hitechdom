package com.mintrocket.data.api.request.auth

import com.google.gson.annotations.SerializedName

class SignInRequest(
    @SerializedName("username") val username: String,
    @SerializedName("password") val password: String
)