package com.mintrocket.data.api.service

import com.mintrocket.data.api.request.managmentCompany.FindByAddressRequest
import com.mintrocket.data.api.response.management_company.FindByAddressResponse
import com.mintrocket.data.api.response.common.BaseResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface ManagementCompanyService {

    @POST("management_companies/findByAddress")
    fun findByAddress(@Body request: FindByAddressRequest): Single<BaseResponse<FindByAddressResponse>>

}