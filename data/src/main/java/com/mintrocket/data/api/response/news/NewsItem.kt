package com.mintrocket.data.api.response.news

import com.google.gson.annotations.SerializedName

class NewsItem(
    @SerializedName("id") val id: Long,
    @SerializedName("name") val name: String,
    @SerializedName("content") val content: String,
    @SerializedName("author") val author: String,
    @SerializedName("published_at") val publishedAt: Long
)