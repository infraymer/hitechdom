package com.mintrocket.data.api.converter

import com.mintrocket.data.api.response.management_company.FindByAddressResponse
import com.mintrocket.domain.entity.management_company.ManagementCompany

fun FindByAddressResponse.toDomain() = ManagementCompany(
    id, name, email, createdAt, updatedAt
)