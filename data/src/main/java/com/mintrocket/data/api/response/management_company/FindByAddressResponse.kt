package com.mintrocket.data.api.response.management_company

import com.google.gson.annotations.SerializedName

class FindByAddressResponse(
    @SerializedName("id") val id: Long,
    @SerializedName("name") val name: String,
    @SerializedName("email") val email: String,
    @SerializedName("created_at") val createdAt: Long,
    @SerializedName("updated_at") val updatedAt: Long
)