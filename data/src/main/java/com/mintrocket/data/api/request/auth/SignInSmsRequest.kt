package com.mintrocket.data.api.request.auth

import com.google.gson.annotations.SerializedName

class SignInSmsRequest(
    @SerializedName("phone") val phone: String,
    @SerializedName("sms") val code: String
)