package com.mintrocket.data

import android.content.Context
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.mintrocket.data.api.converter.toDomain
import com.mintrocket.data.api.response.common.EmptyResponse
import com.mintrocket.domain.DataException
import com.mintrocket.domain.IDataErrorHandler
import retrofit2.HttpException
import java.io.IOException

class DataErrorHandler(
    private val context: Context,
    private val gson: Gson
) : IDataErrorHandler {


    override fun proceed(error: Throwable): String {
        return error.userMessage()
    }

    private fun Throwable.userMessage(): String = when (this) {
        is HttpException -> {
            val errorBody = response().errorBody()
            if (errorBody != null) {
                try {
                    val json = errorBody.string()
                    val apiResponse = gson.fromJson(json, EmptyResponse::class.java)
                    apiResponse.errors?.toDomain()?.userMessage().orEmpty()
                } catch (e: JsonSyntaxException) {
                    context.getString(R.string.unknown_error)
                }
            } else {
                when (this.code()) {
                    304 -> context.getString(R.string.not_modified_error)
                    400 -> context.getString(R.string.bad_request_error)
                    401 -> context.getString(R.string.unauthorized_error)
                    403 -> context.getString(R.string.forbidden_error)
                    404 -> context.getString(R.string.not_found_error)
                    405 -> context.getString(R.string.method_not_allowed_error)
                    409 -> context.getString(R.string.conflict_error)
                    422 -> context.getString(R.string.unprocessable_error)
                    500 -> context.getString(R.string.server_error_error)
                    else -> context.getString(R.string.unknown_error)
                }
            }
        }
        is IOException -> context.getString(R.string.network_error)
        is DataException -> userMessage()
        else -> message.orEmpty()
    }

    /*private fun DataError.userMessage() = when {
        !message.isNullOrBlank() -> message.orEmpty()
        !text.isNullOrBlank() -> text.orEmpty()
        else -> context.getString(R.string.unknown_error)
    }*/

    private fun DataException.userMessage(): String {
        var message = ""
        errors.forEach { message += "${it.text}. " }
        return message
    }
}
