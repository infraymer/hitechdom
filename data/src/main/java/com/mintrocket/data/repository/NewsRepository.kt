package com.mintrocket.data.repository

import com.mintrocket.data.api.converter.toDomain
import com.mintrocket.data.api.request.news.NewsRequest
import com.mintrocket.data.api.response.common.BaseResponse
import com.mintrocket.data.api.service.NewsService
import com.mintrocket.domain.entity.news.News
import com.mintrocket.domain.repository.INewsRepository
import io.reactivex.Single

class NewsRepository(
    private val newsService: NewsService
) : INewsRepository {

    override fun getNews(page: Int, archive: Boolean): Single<List<News>> = newsService
        .getNews(NewsRequest(page, if (archive) "1" else null))
        .compose(BaseResponse.fetchResult())
        .map { newsResponse -> newsResponse.items.map { it.toDomain() } }

    override fun getDetailNews(id: Long): Single<News> = newsService
        .getDetailNews(id)
        .compose(BaseResponse.fetchResult())
        .map { it.toDomain() }

}