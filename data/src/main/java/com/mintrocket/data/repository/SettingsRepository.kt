package com.mintrocket.data.repository

import com.mintrocket.data.storage.IAppStorage
import com.mintrocket.domain.repository.ISettingsRepository

class SettingsRepository(
    private val appStorage: IAppStorage
) : ISettingsRepository {

    override var isDarkTheme: Boolean
        get() = appStorage.isDarkTheme
        set(value) {
            appStorage.isDarkTheme = value
        }
}