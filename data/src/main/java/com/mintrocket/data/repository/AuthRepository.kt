package com.mintrocket.data.repository

import com.mintrocket.data.api.converter.toDomain
import com.mintrocket.data.api.request.auth.*
import com.mintrocket.data.api.response.auth.SignInResponse
import com.mintrocket.data.api.response.common.BaseResponse
import com.mintrocket.data.api.response.common.EmptyResponse
import com.mintrocket.data.api.service.AuthService
import com.mintrocket.data.storage.IAuthStorage
import com.mintrocket.domain.entity.auth.*
import com.mintrocket.domain.repository.IAuthRepository
import io.reactivex.Completable
import io.reactivex.Single
import java.util.concurrent.TimeUnit

class AuthRepository(
    private val authService: AuthService,
    private val authStorage: IAuthStorage
) : IAuthRepository {


    private fun saveAuthData(signInResponse: SignInResponse) {
        authStorage.accessToken = signInResponse.accessToken
        authStorage.refreshToken = signInResponse.refreshToken
        authStorage.expiresIn = signInResponse.expiresIn
        authStorage.tokenType = signInResponse.tokenType
    }

    override val isAuth: Boolean
        get() = authStorage.accessToken.isNotEmpty()

    override var isRegistered: Boolean
        get() = authStorage.isRegistered
        set(value) {
            authStorage.isRegistered = value
        }

    override fun logOut(): Completable = Completable.create {
        authStorage.accessToken = ""
        authStorage.refreshToken = ""
        authStorage.expiresIn = 0
        authStorage.tokenType = ""
        authStorage.isRegistered = false
        it.onComplete()
    }

    override fun signInByUsername(username: String, password: String): Completable = authService
        .signInByUsername(SignInRequest(username, password))
        .compose(BaseResponse.fetchResult())
        .map { saveAuthData(it) }
        .ignoreElement()

    override fun signInBySmsCode(phone: String, code: String): Single<Auth> = authService
        .signInBySmsCode(SignInSmsRequest(phone, code))
        .compose(BaseResponse.fetchResult())
        .map {
            saveAuthData(it)
            it.toDomain()
        }

    override fun sendSmsCode(phone: String): Completable = authService
        .sendSms(SendSmsRequest(phone))
        .compose(EmptyResponse.fetchResult())
        .ignoreElement()

    override fun getUserProfile(): Single<User> = authService
        .getUserProfile()
        .compose(BaseResponse.fetchResult())
        .map { it.toDomain() }

    override fun updateUserProfile(userUpdate: UserUpdate): Single<User> = authService
        .updateUserProfile(UpdateUserRequest(
            userUpdate.firstName,
            userUpdate.lastName,
            userUpdate.secondName
        ))
        .compose(BaseResponse.fetchResult())
        .map { it.toDomain() }

    override fun registration(data: Registration): Completable = authService
        .registration(RegistrationRequest(
            data.firstName,
            data.secondName,
            data.lastName,
            data.city,
            data.street,
            data.house,
            data.housing,
            data.flat
        ))
        .compose(EmptyResponse.fetchResult())
        .ignoreElement()

    /*override fun getPolicy(): Single<Policy> = Single
        .timer(1, TimeUnit.SECONDS)
        .map {
            Policy("В отделение полиции доставлены дворники, 23 декабря избившие двух прохожих в центре Тюмени. Силовики проводят проверку их действий. Об этом «URA.RU» сообщили в региональном УМВД.\n" +
                "«Дворники доставлены в ГОМ №4. \\n\\nПока от противоположной стороны не поступало обращений в полицию и в медицинские учреждения за помощью», — рассказала агентству начальник отдела информации и общественных связей ведомства Светлана Новик. По ее словам, работникам коммунальной службы может грозить наказание за мелкое хулиганство.\n" +
                "Видео вчерашнего инцидента горожане распространили в социальных сетях. На нем видно, как трое людей в оранжевых жилетах черенками и лопатами жестоко избили двух парней. Ранее в мэрии Тюмени «URA.RU» объяснили, что коммунальщики защищались от пьяных хулиганов.")
        }*/

    override fun getPolicy(): Single<Policy> = authService
        .getPolicy()
        .compose(BaseResponse.fetchResult())
        .map { it.toDomain() }
}