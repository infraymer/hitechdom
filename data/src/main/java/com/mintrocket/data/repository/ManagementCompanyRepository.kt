package com.mintrocket.data.repository

import com.mintrocket.data.api.converter.toDomain
import com.mintrocket.data.api.request.managmentCompany.FindByAddressRequest
import com.mintrocket.data.api.response.common.BaseResponse
import com.mintrocket.data.api.service.ManagementCompanyService
import com.mintrocket.domain.entity.Address
import com.mintrocket.domain.entity.Entity
import com.mintrocket.domain.entity.management_company.ManagementCompany
import com.mintrocket.domain.repository.IManagementCompanyRepository
import io.reactivex.Single

class ManagementCompanyRepository(
    private val companyService: ManagementCompanyService
) : IManagementCompanyRepository {

    override fun findByAddress(address: Address): Single<Entity<ManagementCompany>> = companyService
        .findByAddress(FindByAddressRequest(
            address.city,
            address.street,
            address.house,
            address.housing,
            address.flat
        ))
        .compose(BaseResponse.fetchResultNullable())
        .map { Entity(it.item?.toDomain()) }
}