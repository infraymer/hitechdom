package com.mintrocket.domain

class DataError constructor(
        val code: String?,
        override val message: String?,
        val text: String?
) : Throwable() {

    companion object {
        const val WRONG_SMS = "WRONG_SMS"
    }
}
