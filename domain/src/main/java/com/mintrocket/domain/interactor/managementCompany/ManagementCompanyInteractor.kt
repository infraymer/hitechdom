package com.mintrocket.domain.interactor.managementCompany

import com.mintrocket.domain.entity.Address
import com.mintrocket.domain.entity.Entity
import com.mintrocket.domain.entity.management_company.ManagementCompany
import com.mintrocket.domain.repository.IManagementCompanyRepository
import io.reactivex.Single

class ManagementCompanyInteractor(
    private val companyRepository: IManagementCompanyRepository
) : IManagementCompanyInteractor {

    override fun findCompanyByAddress(address: Address): Single<Entity<ManagementCompany>> = companyRepository
        .findByAddress(address)
}