package com.mintrocket.domain.interactor.news

import com.mintrocket.domain.entity.news.News
import io.reactivex.Single

interface INewsInteractor {

    fun getNews(refresh: Boolean = false): Single<List<News>>

    fun getNewsArchive(refresh: Boolean = false): Single<List<News>>

    fun getDetailNews(newsId: Long): Single<News>
}