package com.mintrocket.domain.interactor.managementCompany

import com.mintrocket.domain.entity.Address
import com.mintrocket.domain.entity.Entity
import com.mintrocket.domain.entity.management_company.ManagementCompany
import io.reactivex.Single

interface IManagementCompanyInteractor {

    fun findCompanyByAddress(address: Address): Single<Entity<ManagementCompany>>
}