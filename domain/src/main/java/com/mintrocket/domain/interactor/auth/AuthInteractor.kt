package com.mintrocket.domain.interactor.auth

import com.mintrocket.domain.entity.auth.Policy
import com.mintrocket.domain.entity.auth.Registration
import com.mintrocket.domain.entity.management_company.ManagementCompany
import com.mintrocket.domain.repository.IAuthRepository
import io.reactivex.Completable
import io.reactivex.Single

class AuthInteractor(
    private val authRepository: IAuthRepository
) : IAuthInteractor {

    override var phone: String = ""
    override var registration: Registration? = null
    override var managementCompany: ManagementCompany? = null

    override val isAuth: Boolean
        get() = authRepository.isAuth

    override var isRegistered: Boolean
        get() = authRepository.isRegistered
        set(value) {
            authRepository.isRegistered = value
        }

    override fun logOut(): Completable = authRepository.logOut()

    override fun signInByUsername(username: String, password: String) = authRepository.signInByUsername(username, password)

    override fun signInBySmsCode(phone: String, code: String) = authRepository.signInBySmsCode(phone, code)

    override fun sendSmsCode(phone: String) = authRepository.sendSmsCode(phone)

    override fun getUserProfile() = authRepository.getUserProfile()

    override fun registration(): Completable = authRepository.registration(registration!!)

    override fun getPolicy(): Single<Policy> = authRepository.getPolicy()
}