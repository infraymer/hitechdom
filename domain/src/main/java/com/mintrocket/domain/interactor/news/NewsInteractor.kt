package com.mintrocket.domain.interactor.news

import com.mintrocket.domain.entity.news.News
import com.mintrocket.domain.repository.INewsRepository
import io.reactivex.Single

class NewsInteractor(
    private val newsRepository: INewsRepository
) : INewsInteractor {

    private var currentPage = 1

    override fun getNews(refresh: Boolean): Single<List<News>> {
        if (refresh) currentPage = 1
        return newsRepository
            .getNews(currentPage++)
    }

    override fun getNewsArchive(refresh: Boolean): Single<List<News>> {
        if (refresh) currentPage = 1
        return newsRepository
            .getNews(currentPage++, true)
    }

    override fun getDetailNews(newsId: Long): Single<News> = newsRepository
        .getDetailNews(newsId)
}