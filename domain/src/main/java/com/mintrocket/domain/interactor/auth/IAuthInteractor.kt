package com.mintrocket.domain.interactor.auth

import com.mintrocket.domain.entity.auth.Auth
import com.mintrocket.domain.entity.auth.Policy
import com.mintrocket.domain.entity.auth.Registration
import com.mintrocket.domain.entity.auth.User
import com.mintrocket.domain.entity.management_company.ManagementCompany
import com.sun.org.apache.xpath.internal.operations.Bool
import io.reactivex.Completable
import io.reactivex.Single

interface IAuthInteractor {

    companion object {
        const val RETRY_REQUEST_TIME = 10L
    }

    val isAuth: Boolean
    var isRegistered: Boolean

    var phone: String
    var registration: Registration?
    var managementCompany: ManagementCompany?

    fun logOut(): Completable
    fun signInByUsername(username: String, password: String): Completable
    fun signInBySmsCode(phone: String, code: String): Single<Auth>
    fun sendSmsCode(phone: String): Completable
    fun getUserProfile(): Single<User>
    fun registration(): Completable
    fun getPolicy(): Single<Policy>
}