package com.mintrocket.domain.entity

data class Address(
    val city: String,
    val street: String,
    val house: String,
    val housing: String,
    val flat: String
)