package com.mintrocket.domain.entity.auth

data class Entrance(
    val id: Long,
    val number: String?,
    val storeys: Int?,
    val createdAt: Long?,
    val updatedAt: Long?,
    val house: House?
)