package com.mintrocket.domain.entity.auth

data class Auth(
    val user: User,
    val isRegistered: Boolean
)