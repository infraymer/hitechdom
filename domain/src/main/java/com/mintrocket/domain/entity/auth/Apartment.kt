package com.mintrocket.domain.entity.auth

data class Apartment(
    val id: Long,
    val number: String?,
    val createdAt: Long?,
    val entrance: Entrance?
)