package com.mintrocket.domain.entity.auth

data class Registration(
    val firstName: String,
    val secondName: String,
    val lastName: String,
    val city: String,
    val street: String,
    val house: String,
    val housing: String,
    val flat: String
)