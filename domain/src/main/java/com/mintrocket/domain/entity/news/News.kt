package com.mintrocket.domain.entity.news

data class News(
    val id: Long,
    val name: String,
    val content: String,
    val author: String,
    val publishedAt: Long
)