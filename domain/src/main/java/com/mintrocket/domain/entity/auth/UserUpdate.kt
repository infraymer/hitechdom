package com.mintrocket.domain.entity.auth

data class UserUpdate(
    val firstName: String,
    val secondName: String,
    val lastName: String
)