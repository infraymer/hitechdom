package com.mintrocket.domain.entity.auth

import com.mintrocket.domain.entity.management_company.ManagementCompany

data class House(
    val id: Long,
    val street: String?,
    val house: String?,
    val housing: String?,
    val createdAt: Long?,
    val updatedAt: Long?,
    val managementCompany: ManagementCompany?
)