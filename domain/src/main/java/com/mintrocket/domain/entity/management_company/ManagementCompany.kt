package com.mintrocket.domain.entity.management_company

class ManagementCompany(
    val id: Long,
    val name: String?,
    val email: String?,
    val createdAt: Long?,
    val updatedAt: Long?
)