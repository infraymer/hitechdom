package com.mintrocket.domain.entity.auth

data class Policy(
    val name: String?,
    val content: String?
)