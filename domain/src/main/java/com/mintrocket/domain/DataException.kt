package com.mintrocket.domain

class DataException(
    val errors: List<DataError>
) : Throwable()