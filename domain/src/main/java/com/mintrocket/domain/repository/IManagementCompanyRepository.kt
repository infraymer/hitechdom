package com.mintrocket.domain.repository

import com.mintrocket.domain.entity.Address
import com.mintrocket.domain.entity.Entity
import com.mintrocket.domain.entity.management_company.ManagementCompany
import io.reactivex.Single

interface IManagementCompanyRepository {

    fun findByAddress(address: Address): Single<Entity<ManagementCompany>>
}