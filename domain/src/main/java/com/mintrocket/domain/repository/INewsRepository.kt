package com.mintrocket.domain.repository

import com.mintrocket.domain.entity.news.News
import io.reactivex.Single

interface INewsRepository {

    fun getNews(page: Int, archive: Boolean = false): Single<List<News>>

    fun getDetailNews(id: Long): Single<News>
}