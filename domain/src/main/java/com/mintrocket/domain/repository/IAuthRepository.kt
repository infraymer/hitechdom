package com.mintrocket.domain.repository

import com.mintrocket.domain.entity.auth.*
import io.reactivex.Completable
import io.reactivex.Single

interface IAuthRepository {

    val isAuth: Boolean
    var isRegistered: Boolean

    fun logOut(): Completable

    fun signInByUsername(username: String, password: String): Completable

    fun signInBySmsCode(phone: String, code: String): Single<Auth>

    fun sendSmsCode(phone: String): Completable

    fun getUserProfile(): Single<User>

    fun updateUserProfile(userUpdate: UserUpdate): Single<User>

    fun registration(data: Registration): Completable

    fun getPolicy(): Single<Policy>
}