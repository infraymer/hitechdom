package com.mintrocket.domain.repository

interface ISettingsRepository {

    var isDarkTheme: Boolean
}