package com.mintrocket.domain

import com.mintrocket.domain.interactor.auth.AuthInteractor
import com.mintrocket.domain.interactor.auth.IAuthInteractor
import com.mintrocket.domain.interactor.managementCompany.IManagementCompanyInteractor
import com.mintrocket.domain.interactor.managementCompany.ManagementCompanyInteractor
import com.mintrocket.domain.interactor.news.INewsInteractor
import com.mintrocket.domain.interactor.news.NewsInteractor
import org.koin.dsl.module.Module
import org.koin.dsl.module.module

val domainModule: Module = module {

    single { AuthInteractor(get()) as IAuthInteractor }
    factory { NewsInteractor(get()) as INewsInteractor }
    single { ManagementCompanyInteractor(get()) as IManagementCompanyInteractor }
}