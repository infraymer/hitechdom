package com.mintrocket.domain

interface IDataErrorHandler {
    fun proceed(error: Throwable): String
}
