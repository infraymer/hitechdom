package com.mintrocket.presentation.controller

import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable

class ProgressController {

    private val progressRelay = PublishRelay.create<Boolean>()

    val progress: Observable<Boolean> = progressRelay.hide()
    fun show(show: Boolean = true) = progressRelay.accept(show)
}