package com.mintrocket.presentation.controller

import com.jakewharton.rxrelay2.BehaviorRelay
import com.mintrocket.presentation.entity.UserData
import io.reactivex.Observable

class UserDataController {

    private val userRelay = BehaviorRelay.create<UserData>()

    val user: Observable<UserData> = userRelay.hide()
    fun set(user: UserData) = userRelay.accept(user)
}