package com.mintrocket.presentation.screen.global

import android.graphics.Rect
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View

class UniversalItemDecoration : RecyclerView.ItemDecoration() {
    private var manager: GridLayoutManager? = null
    private var fullWidth: Boolean = false
    private var includeEdge: Boolean = true
    private var spacing: Int = 0

    fun manager(manager: GridLayoutManager): UniversalItemDecoration {
        this.manager = manager
        return this
    }

    fun fullWidth(fullWidth: Boolean): UniversalItemDecoration {
        this.fullWidth = fullWidth
        return this
    }

    fun includeEdge(includeEdge: Boolean): UniversalItemDecoration {
        this.includeEdge = includeEdge
        return this
    }

    fun spacing(spacing: Int): UniversalItemDecoration {
        this.spacing = spacing
        return this
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val position = parent.getChildAdapterPosition(view) // item position
        val defSpanCount = manager?.spanCount ?: 1
        val gridSpanCount = manager?.spanSizeLookup?.getSpanSize(position) ?: defSpanCount

        val spanCount = defSpanCount - gridSpanCount + 1
        val column = position % spanCount // item column

        Log.d("lalala", "getItemOffsets $position, $defSpanCount, $gridSpanCount, $spanCount, $column")

        outRect.apply {
            top = spacing
            right = spacing
            bottom = spacing
            left = spacing
        }

        /*if (includeEdge) {
            if (!fullWidth) {
                outRect.left = spacing - column * spacing / spanCount // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount // (column + 1) * ((1f / spanCount) * spacing)
            }
            if (position < spanCount) { // top edge
                outRect.top = spacing
            }
            outRect.bottom = spacing // item bottom
        } else {
            if (!fullWidth) {
                outRect.left = column * spacing / spanCount // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount // spacing - (column + 1) * ((1f /    spanCount) * spacing)
            }
            if (position >= spanCount) {
                outRect.top = spacing // item top
            }
        }*/
    }
}
