package com.mintrocket.presentation.screen.news

import android.arch.lifecycle.MutableLiveData
import com.mintrocket.domain.interactor.news.INewsInteractor
import com.mintrocket.presentation.common.ActionLiveData
import com.mintrocket.presentation.common.BaseViewModel
import com.mintrocket.presentation.common.ISchedulers
import com.mintrocket.presentation.controller.ProgressController
import com.mintrocket.presentation.converter.toData
import com.mintrocket.presentation.entity.NewsData
import com.mintrocket.presentation.navigation.Screens
import com.mintrocket.presentation.screen.global.IErrorHandler
import ru.terrakok.cicerone.Router

class NewsViewModel(
    private val router: Router,
    private val scheduler: ISchedulers,
    private val newsInteractor: INewsInteractor,
    private val errorHandler: IErrorHandler,
    private val progress: ProgressController
) : BaseViewModel() {

    val newsLiveData = MutableLiveData<List<NewsData>>()
    val moreNewsLiveData = MutableLiveData<List<NewsData>>()
    val progressLiveData = ActionLiveData<Boolean>()
    val detailNewsLiveData = MutableLiveData<NewsData>()


    init {
        /*newsLiveData.value = listOf(
            NewsData(0, "Title 1", "Author 1", 0, ""),
            NewsData(1, "Title 2", "Author 2", 0, ""),
            NewsData(2, "Title 3", "Author 3", 0, ""),
            NewsData(3, "Title 4", "Author 4", 0, ""),
            NewsData(4, "Title 5", "Author 5", 0, ""),
            NewsData(5, "Title 6", "Author 6", 0, ""),
            NewsData(6, "Title 7", "Author 7", 0, ""),
            NewsData(7, "Title 8", "Author 8", 0, ""),
            NewsData(8, "Title 9", "Author 9", 0, "")
        )*/

        getNews()
    }

    private fun getNews(refresh: Boolean = false) = newsInteractor
        .getNews(refresh)
        .subscribeOn(scheduler.io())
        .observeOn(scheduler.ui())
        .doOnSubscribe { progressLiveData.value = true }
        .doAfterTerminate { progressLiveData.value = false }
        .subscribe({
            if (refresh)
                newsLiveData.value = it.map { it.toData() }
            else
                moreNewsLiveData.value = it.map { it.toData() }
        }, {
            errorHandler.proceed(it)
        })
        .addToDisposables()

    fun onRefresh() = getNews(true)

    fun onArchiveClicked() = router.navigateTo(Screens.ArchiveNews)

    fun onItemClicked(item: NewsData) = newsInteractor
        .getDetailNews(item.id)
        .subscribeOn(scheduler.io())
        .observeOn(scheduler.ui())
        .doOnSubscribe { progress.show() }
        .doAfterTerminate { progress.show(false) }
        .subscribe({
            router.navigateTo(Screens.NewsDetail)
            detailNewsLiveData.value = it.toData()
        },
            errorHandler::proceed
        )
        .addToDisposables()

    fun onLoadMore() = getNews()
}