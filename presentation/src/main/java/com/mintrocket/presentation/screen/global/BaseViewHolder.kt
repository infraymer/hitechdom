package com.mintrocket.presentation.screen.global

import android.support.v7.widget.RecyclerView
import android.view.View

open class BaseViewHolder(val itemView: View) : RecyclerView.ViewHolder(itemView)
