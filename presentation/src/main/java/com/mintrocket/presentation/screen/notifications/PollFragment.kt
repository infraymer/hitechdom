package com.mintrocket.presentation.screen.notifications


import android.view.View
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.IItem
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.mikepenz.materialize.holder.StringHolder
import minthouse.mintrocket.com.presentation.R
import com.mintrocket.presentation.common.BaseFragment
import org.koin.android.viewmodel.ext.android.viewModel
import kotlinx.android.synthetic.main.fragment_poll.*
import kotlinx.android.synthetic.main.item_poll.view.*
import java.util.*

class PollFragment : BaseFragment() {

    override val layoutResource = R.layout.fragment_poll

    val viewModel: PollViewModel by viewModel()

    lateinit var fastAdapter: FastAdapter<PollItem>

    override fun initView() {
        val list = listOf<PollItem>(
            PollItem("One"),
            PollItem("Two"),
            PollItem("Three"),
            PollItem("Four")
        )
        val itemAdapter = ItemAdapter<PollItem>()
        fastAdapter = FastAdapter.with(Arrays.asList(itemAdapter))
        recyclerView.adapter = fastAdapter

        itemAdapter.add(list)
    }
}

class PollItem(val name: String) : AbstractItem<PollItem, PollItem.ViewHolder>() {

    override fun getType() = R.id.fastadapter_poll_item_id

    override fun getViewHolder(v: View) = ViewHolder(v)

    override fun getLayoutRes() = R.layout.item_poll

    class ViewHolder(private val view: View) : FastAdapter.ViewHolder<PollItem>(view) {

        override fun unbindView(item: PollItem) {
            view.apply {
                nameTextView.text = null
            }
        }

        override fun bindView(item: PollItem, payloads: MutableList<Any>) {
            view.apply {
                nameTextView.text = item.name
            }
        }
    }
}