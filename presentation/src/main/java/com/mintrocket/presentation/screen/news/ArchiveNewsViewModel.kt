package com.mintrocket.presentation.screen.news

import android.arch.lifecycle.MutableLiveData
import com.mintrocket.domain.interactor.news.INewsInteractor
import com.mintrocket.presentation.common.ActionLiveData
import com.mintrocket.presentation.common.BaseViewModel
import com.mintrocket.presentation.common.ISchedulers
import com.mintrocket.presentation.converter.toData
import com.mintrocket.presentation.entity.NewsData
import com.mintrocket.presentation.screen.global.IErrorHandler
import ru.terrakok.cicerone.Router

class ArchiveNewsViewModel(
    private val router: Router,
    private val scheduler: ISchedulers,
    private val errorHandler: IErrorHandler,
    private val newsInteractor: INewsInteractor
) : BaseViewModel() {

    val newsLiveData = MutableLiveData<List<NewsData>>()
    val moreNewsLiveData = MutableLiveData<List<NewsData>>()
    val progressLiveData = ActionLiveData<Boolean>()

    init {
        getNews()
    }

    private fun getNews(refresh: Boolean = false) = newsInteractor
        .getNewsArchive(refresh)
        .subscribeOn(scheduler.io())
        .observeOn(scheduler.ui())
        .doOnSubscribe { progressLiveData.value = true }
        .doAfterTerminate { progressLiveData.value = false }
        .subscribe({
            if (refresh)
                newsLiveData.value = it.map { it.toData() }
            else
                moreNewsLiveData.value = it.map { it.toData() }
        }, {
            errorHandler.proceed(it)
        })
        .addToDisposables()

    fun onRefresh() = getNews(true)

    fun onItemClicked(item: NewsData) = newsInteractor
        .getDetailNews(item.id)
        .subscribeOn(scheduler.io())
        .observeOn(scheduler.ui())
        .subscribe({

        },
            errorHandler::proceed
        )

    fun onLoadMore() = getNews()
}