package com.mintrocket.presentation.screen.global

interface IErrorHandler {
    fun proceed(error: Throwable)
    fun proceed(error: Throwable, messageListener: (String) -> Unit)
}
