package com.mintrocket.presentation.screen.home

import android.arch.lifecycle.MutableLiveData
import com.mintrocket.domain.interactor.auth.IAuthInteractor
import com.mintrocket.presentation.common.BaseViewModel
import com.mintrocket.presentation.common.ISchedulers
import com.mintrocket.presentation.controller.ProgressController
import com.mintrocket.presentation.controller.UserDataController
import com.mintrocket.presentation.converter.toData
import com.mintrocket.presentation.entity.UserData
import com.mintrocket.presentation.screen.global.IErrorHandler
import ru.terrakok.cicerone.Router

class HomeViewModel(
    private val router: Router,
    private val scheduler: ISchedulers,
    private val authInteractor: IAuthInteractor,
    private val progress: ProgressController,
    private val errorHandler: IErrorHandler,
    private val userController: UserDataController
) : BaseViewModel() {

    val userLiveData = MutableLiveData<UserData>()

    init {
        authInteractor
            .getUserProfile()
            .subscribeOn(scheduler.io())
            .observeOn(scheduler.ui())
//            .doOnSubscribe { progress.show() }
//            .doAfterTerminate { progress.show(false) }
            .subscribe({
                 it.toData().apply {
                     userLiveData.value = this
                     userController.set(this)
                }
            },
                errorHandler::proceed
            )
            .addToDisposables()
    }
}