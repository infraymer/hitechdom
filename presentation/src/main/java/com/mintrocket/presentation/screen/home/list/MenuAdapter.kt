package com.mintrocket.presentation.screen.home.list

import com.mintrocket.presentation.entity.MenuData
import com.mintrocket.presentation.screen.global.BaseDelegationAdapter
import com.mintrocket.presentation.screen.global.MenuItem

class MenuAdapter(
    onClick: (MenuData) -> Unit
) : BaseDelegationAdapter() {

    init {
        addDelegate(MenuDelegate(onClick))
    }

    fun bindItems(newItems: List<MenuData>) {
        items.clear()
        items.addAll(newItems.map { MenuItem(it) })
        notifyDataSetChanged()
    }
}

