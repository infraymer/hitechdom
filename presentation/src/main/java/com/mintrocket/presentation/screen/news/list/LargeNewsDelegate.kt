package com.mintrocket.presentation.screen.news.list

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AbsListItemAdapterDelegate
import com.mintrocket.presentation.entity.NewsData
import com.mintrocket.presentation.extension.asDayString
import com.mintrocket.presentation.extension.asTime
import com.mintrocket.presentation.extension.inflate
import com.mintrocket.presentation.screen.global.ListItem
import com.mintrocket.presentation.screen.global.NewsItem
import kotlinx.android.synthetic.main.item_news_first.view.*
import minthouse.mintrocket.com.presentation.R

class LargeNewsDelegate(
    private val onClick: (NewsData) -> Unit = {}
) : AbsListItemAdapterDelegate<NewsItem, ListItem, LargeNewsDelegate.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): ViewHolder =
        ViewHolder(parent.inflate(R.layout.item_news_first), onClick)


    override fun isForViewType(item: ListItem, items: MutableList<ListItem>, position: Int) =
        position == 0

    override fun onBindViewHolder(item: NewsItem, viewHolder: ViewHolder, payloads: MutableList<Any>) {
        viewHolder.bind(item.data)
    }


    class ViewHolder(
        private val view: View,
        private val onClick: (NewsData) -> Unit
    ) : RecyclerView.ViewHolder(view) {

        fun bind(data: NewsData) {
            view.setOnClickListener { onClick(data) }

            view.apply {
                authorTextView.text = data.author
                titleTextView.text = data.title
                dateTextView.text = data.date.asDayString()
            }
        }
    }
}