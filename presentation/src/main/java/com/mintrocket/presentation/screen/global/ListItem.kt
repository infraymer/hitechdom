package com.mintrocket.presentation.screen.global

import com.mintrocket.presentation.entity.MenuData
import com.mintrocket.presentation.entity.NewsData

sealed class ListItem

class NewsItem(val data: NewsData) : ListItem()

class MenuItem(val data: MenuData) : ListItem()