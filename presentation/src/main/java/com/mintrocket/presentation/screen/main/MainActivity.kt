package com.mintrocket.presentation.screen.main

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import android.widget.Toast
import co.zsmb.materialdrawerkt.builders.drawer
import co.zsmb.materialdrawerkt.draweritems.badgeable.primaryItem
import co.zsmb.materialdrawerkt.draweritems.switchable.switchItem
import com.mikepenz.materialdrawer.Drawer
import com.mintrocket.domain.repository.ISettingsRepository
import com.mintrocket.presentation.common.ISchedulers
import com.mintrocket.presentation.common.MenuViewModel
import com.mintrocket.presentation.common.ProgressDialog
import com.mintrocket.presentation.common.message.SystemMessageNotifier
import com.mintrocket.presentation.common.message.SystemMessageType
import com.mintrocket.presentation.controller.ProgressController
import com.mintrocket.presentation.extension.attr
import com.mintrocket.presentation.extension.subscribe
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import minthouse.mintrocket.com.presentation.R
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity() {

    private val viewModel by viewModel<MainViewModel>()
    val menuModel: MenuViewModel by viewModel()

    private val navigatorHolder by inject<NavigatorHolder>()

    private val navigator =
        object : SupportAppNavigator(this, supportFragmentManager, R.id.container) {

            override fun setupFragmentTransaction(
                command: Command?,
                currentFragment: Fragment?,
                nextFragment: Fragment?,
                fragmentTransaction: FragmentTransaction?) {

                /*fragmentTransaction?.setCustomAnimations(
                    R.anim.slide_from_right,
                    R.anim.slide_to_left,
                    R.anim.slide_from_left,
                    R.anim.slide_to_right
                )*/
            }
        }

    private var notifierDisposable: Disposable? = null
    private var progressDisposable: Disposable? = null

    private val systemMessageNotifier by inject<SystemMessageNotifier>()
    private val progressController by inject<ProgressController>()
    private val scheduler by inject<ISchedulers>()

    private val settingsRepository by inject<ISettingsRepository>()

    var drawer: Drawer? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setTheme(
            if (settingsRepository.isDarkTheme)
                R.style.DarkAppTheme_NoActionBar
            else
                R.style.LightAppTheme_NoActionBar
        )

        setContentView(R.layout.activity_main)
        viewModel.create()
        initDrawer()
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
        subscribeOnSystemMessages()
        subscribeOnProgress()
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        unsubscribeOnSystemMessages()
        unsubscribeOnProgress()
        super.onPause()
    }

    private fun showAlertMessage(message: String) {}

    private fun showToastMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    private fun subscribeOnSystemMessages() {
        notifierDisposable = systemMessageNotifier.notifier
            .subscribe { msg ->
                when (msg.type) {
                    SystemMessageType.ALERT -> showAlertMessage(msg.text)
                    SystemMessageType.TOAST -> showToastMessage(msg.text)
                }
            }
    }

    private fun unsubscribeOnSystemMessages() {
        notifierDisposable?.dispose()
    }

    private fun subscribeOnProgress() {
        progressDisposable = progressController.progress
            .subscribeOn(scheduler.io())
            .observeOn(scheduler.ui())
            .subscribe { showProgressDialog(it) }
    }

    private fun unsubscribeOnProgress() {
        progressDisposable?.dispose()
    }

    private fun showProgressDialog(progress: Boolean) {
        val fragment = supportFragmentManager.findFragmentByTag("bf_progress")
        if (fragment != null && !progress) {
            (fragment as ProgressDialog).dismissAllowingStateLoss()
            supportFragmentManager.executePendingTransactions()
        } else if (fragment == null && progress) {
            ProgressDialog().show(supportFragmentManager, "bf_progress")
            supportFragmentManager.executePendingTransactions()
        }
    }

    private fun initDrawer() {
        menuModel.drawerItemsLiveData.subscribe(this) {
            drawer = drawer {
                headerDivider = false
                footerDivider = false
                stickyHeaderRes = R.layout.layout_drawer_header
                stickyHeaderShadow = false
                selectedItem = -1
                it.forEach {
                    primaryItem(it.name) {
                        selectable = false
                        iconRes = it.iconResource ?: 0
                        iconColorRes = attr(R.attr.text_50).resourceId
                        iconTintingEnabled = true
                        onClick { _ ->
                            menuModel.onItemClicked(it)
                            false
                        }
                    }
                }

                switchItem("Темная тема") {
                    selectable = false
                    checked = settingsRepository.isDarkTheme
                    onSwitchChanged { drawerItem, button, isEnabled ->
                        settingsRepository.isDarkTheme = isEnabled

                        Observable
                            .timer(1, TimeUnit.SECONDS)
                            .subscribe { applyTheme() }
                    }
                }

                primaryItem("Выйти") {
                    selectable = false
                    onClick { _ ->
                        menuModel.onLogOutClicked()
                        false
                    }
                }
                stickyFooterRes = R.layout.layout_drawer_footer
                stickyFooterDivider = false
                stickyFooterShadow = false
            }
        }
        menuModel.userLiveData.subscribe(this) {
            drawer?.stickyFooter?.apply {
                findViewById<TextView>(R.id.titleTextView).text = "${it.firstName} ${it.lastName}"
                findViewById<TextView>(R.id.subtitleTextView).text = it.apartments?.firstOrNull()?.fullAddress
            }
        }
    }

    fun setDrawerLockMode(lock: Boolean) {
        drawer?.drawerLayout?.setDrawerLockMode(
            if (lock)
                DrawerLayout.LOCK_MODE_LOCKED_CLOSED
            else
                DrawerLayout.LOCK_MODE_UNLOCKED
        )
    }

    fun restart() {
        val mStartActivity = Intent(this, MainActivity::class.java)
        val mPendingIntentId = 123456
        val mPendingIntent = PendingIntent.getActivity(this, mPendingIntentId, mStartActivity,
            PendingIntent.FLAG_CANCEL_CURRENT)
        val mgr = this.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent)
        System.exit(0)
    }

    private fun applyTheme() {
        setTheme(
            if (settingsRepository.isDarkTheme)
                R.style.DarkAppTheme_NoActionBar
            else
                R.style.LightAppTheme_NoActionBar
        )
        restartActivity()
        val enterAnimation = android.R.anim.fade_in
        val exitAnimation = android.R.anim.fade_out
        overridePendingTransition(enterAnimation, exitAnimation)
    }

    private fun restartActivity() {
        val intent = intent
        intent.removeCategory(Intent.CATEGORY_LAUNCHER)
        startActivity(intent)
        finish()
    }
}