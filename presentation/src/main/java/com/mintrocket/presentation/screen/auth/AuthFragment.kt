package com.mintrocket.presentation.screen.auth

import android.graphics.Paint
import android.support.constraint.ConstraintSet
import android.view.View
import com.mintrocket.presentation.InputMask
import com.mintrocket.presentation.common.BaseFragment
import com.mintrocket.presentation.extension.dip
import com.mintrocket.presentation.extension.visible
import com.redmadrobot.inputmask.MaskedTextChangedListener
import kotlinx.android.synthetic.main.fragment_auth.*
import minthouse.mintrocket.com.presentation.R
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent
import net.yslibrary.android.keyboardvisibilityevent.Unregistrar
import org.koin.android.viewmodel.ext.android.viewModel

class AuthFragment : BaseFragment() {

    override val layoutResource = R.layout.fragment_auth

    private lateinit var unregistrar: Unregistrar

    val viewModel: AuthViewModel by viewModel()

    private var currentTextActionButton = ""
    private val actionButtonClickListener = View.OnClickListener { viewModel.onActionClicked() }
    private val resendButtonClickListener = View.OnClickListener { viewModel.onResendCodeClicked() }

    override fun initView() {
        unregistrar = KeyboardVisibilityEvent.registerEventListener(activity) {
            logoImageView.visible(!it)
            logoTextView.visible(!it)
            val set = ConstraintSet()
            set.clone(constraintLayout)
            set.setVerticalBias(R.id.actionButton, if (it) 0f else 1f)
            set.applyTo(constraintLayout)
        }
        initPhone()
        initCode()

        viewModel.stateLiveData.subscribe { setState(it) }
        viewModel.progressLiveData.subscribe { showProgress(it) }
        viewModel.timeState.subscribe { setTime(it) }
        viewModel.startTimerLiveData.subscribe { enableResendButton(it) }
        viewModel.errorCodeLiveData.subscribe { errorCode(it) }

        actionButton.setOnClickListener(actionButtonClickListener)
        resendCodeButton.setOnClickListener(resendButtonClickListener)
        changeButton.setOnClickListener { viewModel.onChangeClicked() }

        resendCodeButton.paintFlags = resendCodeButton.paintFlags or Paint.UNDERLINE_TEXT_FLAG
    }

    private fun showProgress(show: Boolean = true) {
        if (show) {
            val set = ConstraintSet()
            set.clone(constraintLayout)
            set.constrainWidth(R.id.actionButton, dip(52))
            set.applyTo(constraintLayout)
            currentTextActionButton = actionButton.text.toString()
            actionButton.text = ""
            actionButton.setOnClickListener(null)
        } else {
            val set = ConstraintSet()
            set.clone(constraintLayout)
            set.constrainWidth(R.id.actionButton, 0)
            set.applyTo(constraintLayout)
            actionButton.text = currentTextActionButton
            actionButton.setOnClickListener(actionButtonClickListener)
        }
        progress.visible(show)
    }

    private fun initPhone() {
        MaskedTextChangedListener.installOn(
            numberEditText,
            InputMask.PHONE,
            object : MaskedTextChangedListener.ValueListener {
                override fun onTextChanged(maskFilled: Boolean, extractedValue: String) {
                    actionButton.isEnabled = maskFilled
                    viewModel.onPhoneEdit(extractedValue)
                }
            }
        )
    }

    private fun initCode() {
        MaskedTextChangedListener.installOn(
            codeEditText,
            InputMask.SMS_CODE,
            object : MaskedTextChangedListener.ValueListener {
                override fun onTextChanged(maskFilled: Boolean, extractedValue: String) {
                    actionButton.isEnabled = maskFilled
                    viewModel.onCodeEdit(extractedValue)
                }
            }
        )
    }

    private fun errorCode(isEnable: Boolean = true) {
        codeInputLayout.error = getString(R.string.auth_error_code)
        codeInputLayout.isErrorEnabled = isEnable
    }

    private fun setTime(time: Long) {
        val isReadyResend = time == 0L
        resendCodeButton.text = getString(R.string.auth_resend_code) + if (isReadyResend) "" else " ($time)"
        resendCodeButton.alpha = if (isReadyResend) 1f else 0.2f
    }

    private fun enableResendButton(enable: Boolean) {
        resendCodeButton.setOnClickListener(if (!enable) resendButtonClickListener else null)
    }

    private fun setState(code: Boolean) {
        titleTextView.text = if (code) getString(R.string.auth_code) else getString(R.string.auth_sign_in)
        descriptionTextView.text = if (code) getString(R.string.auth_code_description) else getString(R.string.auth_sign_in_description)
        actionButton.text = if (code) getString(R.string.auth_done) else getString(R.string.auth_send_code)
        currentTextActionButton = actionButton.text.toString()
        codeInputLayout.visible(code)
        resendCodeButton.visible(code)
        numberInputLayout.hint = if (code) " " else getString(R.string.auth_phone)
        numberEditText.isClickable = !code
        numberEditText.isFocusable = !code
        numberEditText.isFocusableInTouchMode = !code
        changeButton.visible(code)
        if (code) {
            codeEditText.text.clear()
        } else {
            numberEditText.requestFocus()
        }

        val set = ConstraintSet()
        set.clone(constraintLayout)
        set.setMargin(R.id.numberInputLayout, ConstraintSet.TOP, dip(if (code) 0 else 20))
        set.applyTo(constraintLayout)
    }

    override fun onDestroyView() {
        unregistrar.unregister()
        super.onDestroyView()
    }
}