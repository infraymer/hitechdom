package com.mintrocket.presentation.screen.home


import android.annotation.SuppressLint
import android.support.v7.view.menu.MenuView
import android.support.v7.widget.GridLayoutManager
import com.mintrocket.presentation.common.BaseFragment
import com.mintrocket.presentation.common.DrawerFragment
import com.mintrocket.presentation.common.MenuViewModel
import com.mintrocket.presentation.entity.ApartmentData
import com.mintrocket.presentation.screen.home.list.MenuAdapter
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.layout_drawer_footer.*
import minthouse.mintrocket.com.presentation.R
import org.koin.android.viewmodel.ext.android.viewModel

class HomeFragment : BaseFragment() {

    override val layoutResource = R.layout.fragment_home
    override var unlockDrawer = true

    val viewModel: HomeViewModel by viewModel()
    val menuModel: MenuViewModel by viewModel()

    private val menuAdapter by lazy { MenuAdapter { menuModel.onItemClicked(it) } }


    override fun initView() {
        super.initView()
        recyclerView.apply {
            layoutManager = GridLayoutManager(context, 3)
            adapter = menuAdapter
        }

        menuModel.itemsLiveData.subscribe { menuAdapter.bindItems(it) }
        viewModel.userLiveData.subscribe {
            setFlatText(it.apartments?.firstOrNull())
        }

        backButton.setOnClickListener { openDrawer() }
        flatButton.setOnClickListener { }
    }

    @SuppressLint("SetTextI18n")
    fun setFlatText(apartmentData: ApartmentData?) {
        if (apartmentData == null) {
            flatTextView.text = "Нет квартиры"
            return
        }
        val house = apartmentData.entrance?.house
        val street = house?.street
        val numHouse = house?.house
        val housing = house?.housing
        val flat = apartmentData.number
        flatTextView.text = "ул. $street, д. $numHouse, " +
            (if (housing != null && housing.isNotEmpty()) "корп. $housing, " else "") +
            "кв. $flat"
    }
}