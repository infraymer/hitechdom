package com.mintrocket.presentation.screen.global

import com.mintrocket.presentation.common.message.SystemMessageNotifier
import com.mintrocket.domain.IDataErrorHandler

class ErrorHandler(
    private val systemMessageNotifier: SystemMessageNotifier,
    private val dataErrorHandler: IDataErrorHandler
) : IErrorHandler {

    override fun proceed(error: Throwable) {
        proceed(error) { systemMessageNotifier.send(it) }
    }

    override fun proceed(error: Throwable, messageListener: (String) -> Unit) {
        error.printStackTrace()
        messageListener.invoke(dataErrorHandler.proceed(error))
    }

}
