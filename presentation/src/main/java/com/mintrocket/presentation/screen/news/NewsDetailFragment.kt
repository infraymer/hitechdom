package com.mintrocket.presentation.screen.news


import com.mintrocket.presentation.common.BaseFragment
import com.mintrocket.presentation.extension.asDayString
import com.mintrocket.presentation.extension.setHtmlText
import kotlinx.android.synthetic.main.fragment_news_detail.*
import minthouse.mintrocket.com.presentation.R
import org.koin.android.viewmodel.ext.android.sharedViewModel

class NewsDetailFragment : BaseFragment() {

    override val layoutResource = R.layout.fragment_news_detail

    val viewModel: NewsViewModel by sharedViewModel()

    override fun initView() {
        backButton.setOnClickListener { onBackPressed() }

        viewModel.detailNewsLiveData.subscribe {
            authorTextView.text = it.author
            titleTextView.text = it.title
            dateTextView.text = it.date.asDayString()
            contentTextView.setHtmlText(it.content)
        }
    }
}