package com.mintrocket.presentation.screen.registration


import android.support.v4.content.ContextCompat
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import com.mintrocket.presentation.common.BaseFragment
import com.mintrocket.presentation.entity.RegistrationData
import com.mintrocket.presentation.extension.attr
import kotlinx.android.synthetic.main.fragment_registration.*
import minthouse.mintrocket.com.presentation.R
import org.koin.android.viewmodel.ext.android.viewModel

class RegistrationFragment : BaseFragment() {

    override val layoutResource = R.layout.fragment_registration

    val viewModel: RegistrationViewModel by viewModel()

    override fun initView() {
        initPolicy()

        actionButton.setOnClickListener {
            viewModel.onSignUpClicked(
                RegistrationData(
                    firstNameEditText.text.toString(),
                    secondNameEditText.text.toString(),
                    lastNameEditText.text.toString(),
                    cityNameEditText.text.toString(),
                    streetNameEditText.text.toString(),
                    houseNameEditText.text.toString(),
                    housingNameEditText.text.toString(),
                    flatNameEditText.text.toString()
                )
            )
        }
    }

    private fun initPolicy() {
        val cPrimaryRes = context?.attr(R.attr.cPrimary)?.resourceId ?: 0
        val color = ContextCompat.getColor(context!!, cPrimaryRes)

        val spanString = SpannableString(policyTextView.text)
        spanString.setSpan(UnderlineSpan(), 44, spanString.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        spanString.setSpan(ForegroundColorSpan(color), 44, spanString.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        policyTextView.text = spanString
        policyTextView.setOnClickListener { viewModel.onPolicyClicked() }
    }
}