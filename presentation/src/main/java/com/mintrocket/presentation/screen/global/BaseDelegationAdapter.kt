package com.mintrocket.presentation.screen.global

import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter

open class BaseDelegationAdapter : ListDelegationAdapter<MutableList<ListItem>>() {
    init {
        items = mutableListOf()
    }

    protected fun addDelegate(delegate: AdapterDelegate<MutableList<ListItem>>) {
        delegatesManager.addDelegate(delegate)
    }

    protected fun addDelegate(viewType: Int, delegate: AdapterDelegate<MutableList<ListItem>>) {
        delegatesManager.addDelegate(viewType, delegate)
    }
}
