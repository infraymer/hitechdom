package com.mintrocket.presentation.screen.main

import com.mintrocket.domain.interactor.auth.IAuthInteractor
import com.mintrocket.presentation.common.BaseViewModel
import com.mintrocket.presentation.navigation.Screens
import ru.terrakok.cicerone.Router

class MainViewModel(
    private val router: Router,
    private val authInteractor: IAuthInteractor
) : BaseViewModel() {


    init {

    }

    fun create() {
        val startScreen = when {
            authInteractor.isAuth && authInteractor.isRegistered -> Screens.Home
            authInteractor.isAuth -> Screens.Registration
            else -> Screens.Auth
        }


            /*if (authInteractor.isAuth && authInteractor.isRegistered) {
                Screens.Home
            } else if (authInteractor.isAuth) {
                Screens.Registration
            } else {
                Screens.Auth
            }*/
        router.newRootScreen(Screens.Poll)
//        router.newRootScreen(startScreen)
    }
}
