package com.mintrocket.presentation.screen.news


import com.mintrocket.presentation.common.BaseFragment
import com.mintrocket.presentation.screen.news.list.NewsAdapter
import kotlinx.android.synthetic.main.fragment_news_archive.*
import minthouse.mintrocket.com.presentation.R
import org.koin.android.viewmodel.ext.android.viewModel

class ArchiveNewsFragment : BaseFragment() {

    override val layoutResource = R.layout.fragment_news_archive

    val viewModel: ArchiveNewsViewModel by viewModel()

    private val newsAdapter by lazy {
        NewsAdapter(
            { viewModel.onItemClicked(it) },
            { viewModel.onLoadMore() }
        )
    }

    override fun initView() {
        backButton.setOnClickListener { onBackPressed() }

        viewModel.newsLiveData.subscribe { newsAdapter.bindItems(it) }
        viewModel.moreNewsLiveData.subscribe { newsAdapter.addItems(it) }
        viewModel.progressLiveData.subscribe { swipeRefresh.isRefreshing = it }

        swipeRefresh.setOnRefreshListener { viewModel.onRefresh() }
    }
}