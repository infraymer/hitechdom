package com.mintrocket.presentation.screen.auth

import android.arch.lifecycle.MutableLiveData
import com.mintrocket.domain.DataError
import com.mintrocket.domain.DataException
import com.mintrocket.domain.interactor.auth.IAuthInteractor
import com.mintrocket.presentation.common.ActionLiveData
import com.mintrocket.presentation.common.BaseViewModel
import com.mintrocket.presentation.common.ISchedulers
import com.mintrocket.presentation.common.message.SystemMessageNotifier
import com.mintrocket.presentation.navigation.Screens
import com.mintrocket.presentation.screen.global.IErrorHandler
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import ru.terrakok.cicerone.Router
import java.util.concurrent.TimeUnit

class AuthViewModel(
    private val router: Router,
    private val authInteractor: IAuthInteractor,
    private val errorHandler: IErrorHandler,
    private val scheduler: ISchedulers,
    private val messageNotifier: SystemMessageNotifier
) : BaseViewModel() {

    companion object {
        const val PHONE = false
        const val CODE = true
    }

    val stateLiveData = MutableLiveData<Boolean>()
    val progressLiveData = MutableLiveData<Boolean>()
    val errorCodeLiveData = ActionLiveData<Boolean>()
    val timeState = MutableLiveData<Long>()
    val startTimerLiveData = ActionLiveData<Boolean>()


    private val timerObservable = Observable.interval(0, 1, TimeUnit.SECONDS)
    private var timerDisposable: Disposable? = null

    private var phone: String = ""
    private var code: String = ""

    init {
        stateLiveData.value = PHONE
    }


    fun onActionClicked() {
        if (stateLiveData.value == CODE) return done()
        sendCode()
    }

    fun onChangeClicked() {
        stateLiveData.value = PHONE
    }

    fun onPhoneEdit(s: String) {
        phone = "+7$s"
        authInteractor.phone = phone
    }

    fun onCodeEdit(s: String) {
        code = s
        errorCodeLiveData.value = false
    }

    fun onResendCodeClicked() = sendCode()

    fun startTimer() = timerObservable
        .subscribeOn(scheduler.io())
        .observeOn(scheduler.ui())
        .doOnSubscribe { startTimerLiveData.value = true }
        .doOnDispose { startTimerLiveData.value = false }
        .subscribe {
            if (it == IAuthInteractor.RETRY_REQUEST_TIME) timerDisposable?.dispose()
            timeState.value = IAuthInteractor.RETRY_REQUEST_TIME - it
        }
        .apply { timerDisposable = this }
        .addToDisposables()

    private fun sendCode() = authInteractor
        .sendSmsCode(phone)
        .subscribeOn(scheduler.io())
        .observeOn(scheduler.ui())
        .doOnSubscribe { showProgress() }
        .doAfterTerminate { showProgress(false) }
        .subscribe({
            timerDisposable?.dispose()
            startTimer()
            stateLiveData.value = CODE
        }, {
            errorHandler.proceed(it)
            stateLiveData.value = PHONE
        })
        .addToDisposables()


    private fun done() = authInteractor
        .signInBySmsCode(phone, code)
        .subscribeOn(scheduler.io())
        .observeOn(scheduler.ui())
        .doOnSubscribe { showProgress() }
        .doAfterTerminate { showProgress(false) }
        .subscribe({
            if (it.isRegistered) {
                authInteractor.isRegistered = it.isRegistered
                router.newRootScreen(Screens.Home)
            } else {
                router.navigateTo(Screens.Registration)
            }
        }, {
            if (it is DataException) {
                it.errors
                    .find { it.code == DataError.WRONG_SMS }
                    ?.let { errorCodeLiveData.value = true }
                return@subscribe
            }
            errorHandler.proceed(it)
        })
        .addToDisposables()

    private fun showProgress(show: Boolean = true) {
        progressLiveData.value = show
    }
}