package com.mintrocket.presentation.screen.policy

import android.arch.lifecycle.MutableLiveData
import com.mintrocket.domain.interactor.auth.IAuthInteractor
import com.mintrocket.presentation.common.BaseViewModel
import com.mintrocket.presentation.common.ISchedulers
import com.mintrocket.presentation.controller.ProgressController
import com.mintrocket.presentation.screen.global.IErrorHandler

class PolicyViewModel(
    private val scheduler: ISchedulers,
    private val authInteractor: IAuthInteractor,
    private val errorHandler: IErrorHandler,
    private val progress: ProgressController
) : BaseViewModel() {

    val contentLiveData = MutableLiveData<Pair<String?, String?>>()

    init {
        authInteractor
            .getPolicy()
            .subscribeOn(scheduler.io())
            .observeOn(scheduler.ui())
            .doOnSubscribe { progress.show() }
            .doAfterTerminate { progress.show(false) }
            .subscribe({
                contentLiveData.value = Pair(it.name, it.content)
            },
                errorHandler::proceed
            )
            .addToDisposables()
    }
}