package com.mintrocket.presentation.screen.registration


import android.graphics.Paint
import com.mintrocket.presentation.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_registration_check_data.*
import minthouse.mintrocket.com.presentation.R
import org.koin.android.viewmodel.ext.android.viewModel

class RegistrationCheckFragment : BaseFragment() {

    override val layoutResource = R.layout.fragment_registration_check_data

    val viewModel: RegistrationCheckViewModel by viewModel()

    override fun initView() {
        initNoButton()
        initClickListeners()

        viewModel.checkDataLiveData.subscribe {
            phoneTextView.text = it.first
            val reg = it.second
            ownerTextView.text = "${reg?.lastName} ${reg?.firstName} ${reg?.secondName}"
            addressTextView.text = "г. ${reg?.city}, ул. ${reg?.street}, д. ${reg?.house}, " +
                "${if (reg?.housing != null && reg?.housing?.isNotEmpty()) "корп. ${reg?.housing}, " else ""}" +
                "кв. ${reg?.flat}"
            companyTextView.text = it.third?.name ?: "Не найдено"
        }
    }

    private fun initNoButton() {
        noButton.paintFlags = noButton.paintFlags or Paint.UNDERLINE_TEXT_FLAG
    }

    private fun initClickListeners() {
        yesButton.setOnClickListener { viewModel.onYesClicked() }
        noButton.setOnClickListener { viewModel.onNoClicked() }
    }
}