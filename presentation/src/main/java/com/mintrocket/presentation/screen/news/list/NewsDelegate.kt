package com.mintrocket.presentation.screen.news.list

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.mintrocket.presentation.entity.NewsData
import com.mintrocket.presentation.extension.asDayString
import com.mintrocket.presentation.extension.asTime
import com.mintrocket.presentation.screen.global.BaseAdapterDelegate
import com.mintrocket.presentation.screen.global.ListItem
import com.mintrocket.presentation.screen.global.NewsItem
import kotlinx.android.synthetic.main.item_news.view.*
import minthouse.mintrocket.com.presentation.R

class NewsDelegate(
    private val onClick: (NewsData) -> Unit = {},
    private val loadMoreItems: () -> Unit = {}
) : BaseAdapterDelegate() {


    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        NewsViewHolder(inflateLayout(parent, R.layout.item_news), onClick)

    override fun isForViewType(items: MutableList<ListItem>, position: Int): Boolean =
        items[position] is NewsItem

    override fun onBindViewHolder(items: MutableList<ListItem>, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        val item = items[position] as NewsItem
        (holder as NewsViewHolder).bind(item.data)
        if (position == items.size - 1) loadMoreItems()
    }

    class NewsViewHolder(
        private val view: View,
        private val onClick: (NewsData) -> Unit
    ) : RecyclerView.ViewHolder(view) {

        fun bind(news: NewsData) {
            view.setOnClickListener { onClick(news) }

            view.apply {
                authorTextView.text = news.author
                titleTextView.text = news.title
                dateTextView.text = news.date.asDayString()
            }
        }
    }
}