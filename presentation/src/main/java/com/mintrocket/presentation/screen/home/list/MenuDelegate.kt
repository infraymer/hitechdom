package com.mintrocket.presentation.screen.home.list

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AbsListItemAdapterDelegate
import com.mintrocket.presentation.entity.MenuData
import com.mintrocket.presentation.extension.inflate
import com.mintrocket.presentation.screen.global.ListItem
import com.mintrocket.presentation.screen.global.MenuItem
import kotlinx.android.synthetic.main.item_menu.view.*
import minthouse.mintrocket.com.presentation.R

class MenuDelegate(
    private val onClick: (MenuData) -> Unit = {}
) : AbsListItemAdapterDelegate<MenuItem, ListItem, MenuDelegate.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): ViewHolder =
        ViewHolder(parent.inflate(R.layout.item_menu), onClick)


    override fun isForViewType(item: ListItem, items: MutableList<ListItem>, position: Int) =
        item is MenuItem

    override fun onBindViewHolder(item: MenuItem, viewHolder: ViewHolder, payloads: MutableList<Any>) {
        viewHolder.bind(item.data)
    }


    class ViewHolder(
        private val view: View,
        private val onClick: (MenuData) -> Unit
    ) : RecyclerView.ViewHolder(view) {

        fun bind(data: MenuData) {
            view.apply {
                name.text = data.name
                icon.setImageResource(data.iconResource ?: 0)

                setOnClickListener { onClick(data) }
            }
        }
    }
}