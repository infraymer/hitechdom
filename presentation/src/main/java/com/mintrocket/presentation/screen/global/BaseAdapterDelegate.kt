package com.mintrocket.presentation.screen.global

import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate

abstract class BaseAdapterDelegate : AdapterDelegate<MutableList<ListItem>>() {
    fun inflateLayout(parent: ViewGroup, @LayoutRes layoutRes: Int): View {
        return LayoutInflater.from(parent.context).inflate(layoutRes, parent, false)
    }
}
