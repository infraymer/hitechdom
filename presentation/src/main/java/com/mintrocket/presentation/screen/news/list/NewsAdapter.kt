package com.mintrocket.presentation.screen.news.list

import com.mintrocket.presentation.entity.NewsData
import com.mintrocket.presentation.screen.global.BaseDelegationAdapter
import com.mintrocket.presentation.screen.global.NewsItem

class NewsAdapter(
    private val onClick: (NewsData) -> Unit,
    private val loadMoreItems: () -> Unit
) : BaseDelegationAdapter() {

    init {
        addDelegate(LargeNewsDelegate(onClick))
        addDelegate(NewsDelegate(onClick, loadMoreItems))
    }

    fun bindItems(newItems: List<NewsData>) {
        items.clear()
        items.addAll(newItems.map { NewsItem(it) })
        notifyDataSetChanged()
    }

    fun addItems(newItems: List<NewsData>) {
        val start = items.size
        val end = start + newItems.size - 1
        items.addAll(newItems.map { NewsItem(it) })
        notifyItemRangeChanged(start, end)
    }
}