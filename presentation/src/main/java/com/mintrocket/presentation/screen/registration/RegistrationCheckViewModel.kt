package com.mintrocket.presentation.screen.registration

import android.arch.lifecycle.MutableLiveData
import com.mintrocket.domain.interactor.auth.IAuthInteractor
import com.mintrocket.presentation.common.BaseViewModel
import com.mintrocket.presentation.common.ISchedulers
import com.mintrocket.presentation.common.message.SystemMessageNotifier
import com.mintrocket.presentation.converter.toData
import com.mintrocket.presentation.entity.ManagementCompanyData
import com.mintrocket.presentation.entity.RegistrationData
import com.mintrocket.presentation.navigation.Screens
import com.mintrocket.presentation.screen.global.IErrorHandler
import ru.terrakok.cicerone.Router

class RegistrationCheckViewModel(
    private val router: Router,
    private val scheduler: ISchedulers,
    private val authInteractor: IAuthInteractor,
    private val errorHandler: IErrorHandler,
    private val notifier: SystemMessageNotifier
) : BaseViewModel() {


    val checkDataLiveData = MutableLiveData<Triple<String, RegistrationData?, ManagementCompanyData?>>()

    init {
        checkDataLiveData.value = Triple(
            authInteractor.phone,
            authInteractor.registration?.toData(),
            authInteractor.managementCompany?.toData()
        )
    }

    fun onYesClicked() {
        authInteractor.isRegistered = true
        router.newRootScreen(Screens.Home)
    }

    fun onNoClicked() {
        router.exit()
    }
}