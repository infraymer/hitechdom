package com.mintrocket.presentation.screen.news


import android.support.v7.widget.LinearLayoutManager
import com.mintrocket.presentation.common.BaseFragment
import com.mintrocket.presentation.common.DrawerFragment
import com.mintrocket.presentation.screen.news.list.NewsAdapter
import kotlinx.android.synthetic.main.fragment_news.*
import minthouse.mintrocket.com.presentation.R
import org.koin.android.viewmodel.ext.android.sharedViewModel

class NewsFragment : BaseFragment() {

    override val layoutResource = R.layout.fragment_news
    override var unlockDrawer = true

    val viewModel: NewsViewModel by sharedViewModel()

    private val newsAdapter by lazy {
        NewsAdapter(
            { viewModel.onItemClicked(it) },
            { viewModel.onLoadMore() }
        )
    }

    override fun initView() {
        super.initView()
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = newsAdapter
        }
        viewModel.newsLiveData.subscribe { newsAdapter.bindItems(it) }
        viewModel.moreNewsLiveData.subscribe { newsAdapter.addItems(it) }
        viewModel.progressLiveData.subscribe { swipeRefresh.isRefreshing = it }

        backButton.setOnClickListener { openDrawer() }
        swipeRefresh.setOnRefreshListener { viewModel.onRefresh() }

        archiveButton.setOnClickListener { viewModel.onArchiveClicked() }
    }
}