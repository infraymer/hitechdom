package com.mintrocket.presentation.screen.registration

import com.mintrocket.domain.interactor.auth.IAuthInteractor
import com.mintrocket.domain.interactor.managementCompany.IManagementCompanyInteractor
import com.mintrocket.presentation.common.BaseViewModel
import com.mintrocket.presentation.common.ISchedulers
import com.mintrocket.presentation.controller.ProgressController
import com.mintrocket.presentation.converter.toAddress
import com.mintrocket.presentation.converter.toDomain
import com.mintrocket.presentation.entity.RegistrationData
import com.mintrocket.presentation.navigation.Screens
import com.mintrocket.presentation.screen.global.IErrorHandler
import ru.terrakok.cicerone.Router

class RegistrationViewModel(
    private val router: Router,
    private val scheduler: ISchedulers,
    private val errorHandler: IErrorHandler,
    private val authInteractor: IAuthInteractor,
    private val companyInteractor: IManagementCompanyInteractor,
    private val progress: ProgressController
) : BaseViewModel() {


    fun onSignUpClicked(data: RegistrationData) {
        authInteractor.registration = data.toDomain()
        authInteractor
            .registration()
            .andThen(companyInteractor.findCompanyByAddress(data.toAddress()))
            .subscribeOn(scheduler.io())
            .observeOn(scheduler.ui())
            .doOnSubscribe { progress.show() }
            .doAfterTerminate { progress.show(false) }
            .subscribe({
                authInteractor.managementCompany = it.item
                router.navigateTo(Screens.RegistrationCheck)
            }, {
                errorHandler.proceed(it)
            })
            .addToDisposables()
    }

    fun onPolicyClicked() {
        router.navigateTo(Screens.Policy)
    }
}