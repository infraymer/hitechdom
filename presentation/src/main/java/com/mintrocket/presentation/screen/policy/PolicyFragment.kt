package com.mintrocket.presentation.screen.policy


import minthouse.mintrocket.com.presentation.R
import com.mintrocket.presentation.common.BaseFragment
import com.mintrocket.presentation.extension.setHtmlText
import com.mintrocket.presentation.extension.visible
import org.koin.android.viewmodel.ext.android.viewModel
import kotlinx.android.synthetic.main.fragment_policy.*

class PolicyFragment : BaseFragment() {

    override val layoutResource = R.layout.fragment_policy

    val viewModel: PolicyViewModel by viewModel()

    override fun initView() {
        backButton.setOnClickListener { onBackPressed() }
        cardView.visible(false)
        viewModel.contentLiveData.subscribe {
            titleTextView.text = it.first
            contentTextView.setHtmlText(it.second ?: "")
            cardView.visible()
        }
    }
}