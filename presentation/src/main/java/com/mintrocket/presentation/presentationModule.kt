package com.mintrocket.presentation

import com.jakewharton.rxrelay2.PublishRelay
import com.mintrocket.presentation.common.ISchedulers
import com.mintrocket.presentation.common.MenuViewModel
import com.mintrocket.presentation.common.Schedulers
import com.mintrocket.presentation.common.message.SystemMessageNotifier
import com.mintrocket.presentation.controller.ProgressController
import com.mintrocket.presentation.controller.UserDataController
import com.mintrocket.presentation.screen.auth.AuthViewModel
import com.mintrocket.presentation.screen.global.ErrorHandler
import com.mintrocket.presentation.screen.global.IErrorHandler
import com.mintrocket.presentation.screen.home.HomeViewModel
import com.mintrocket.presentation.screen.main.MainViewModel
import com.mintrocket.presentation.screen.news.ArchiveNewsViewModel
import com.mintrocket.presentation.screen.news.NewsViewModel
import com.mintrocket.presentation.screen.policy.PolicyViewModel
import com.mintrocket.presentation.screen.registration.RegistrationCheckViewModel
import com.mintrocket.presentation.screen.registration.RegistrationViewModel
import org.koin.android.viewmodel.experimental.builder.viewModel
import org.koin.dsl.module.Module
import org.koin.dsl.module.module
import ru.terrakok.cicerone.Cicerone


const val INST_DRAWER_UPDATE = "inst_drawer_update"

val presentationModule: Module = module {

    val cicerone = Cicerone.create()

    single { cicerone.router }
    single { cicerone.navigatorHolder }

    factory<ISchedulers> { Schedulers() }

    single { SystemMessageNotifier() }
    single { ProgressController() }

    single { UserDataController() }

    single<IErrorHandler> { ErrorHandler(get(), get()) }

    single(name = INST_DRAWER_UPDATE) { PublishRelay.create<Unit>() }

    viewModel<MainViewModel>()
    viewModel<AuthViewModel>()
    viewModel<RegistrationViewModel>()
    viewModel<RegistrationCheckViewModel>()
    viewModel<PolicyViewModel>()
    viewModel<NewsViewModel>()
    viewModel<ArchiveNewsViewModel>()
    viewModel<HomeViewModel>()
    viewModel<MenuViewModel>()
}