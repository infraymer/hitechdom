package com.mintrocket.presentation.common

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.mikepenz.materialdrawer.Drawer
import com.mintrocket.presentation.screen.main.MainActivity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

abstract class BaseFragment : Fragment(), OnBackPressed {

    abstract val layoutResource: Int

    private val router by inject<Router>()

    private val compositeDisposable = CompositeDisposable()

    open var unlockDrawer = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(layoutResource, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val act = activity as MainActivity
        act.setDrawerLockMode(!unlockDrawer)

        initView()
    }

    fun <T> MutableLiveData<T>.subscribe(action: (T) -> Unit) {
        observe(this@BaseFragment, Observer { it?.let { action(it) } })
    }

    open fun initView() = Unit

    open fun openDrawer() {
        (activity as MainActivity).drawer?.openDrawer()
    }

    override fun onBackPressed() = router.exit()

    fun Disposable.addToDisposables() = compositeDisposable.add(this)

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
        hideSoftwareKeyboard()
    }

    protected fun hideSoftwareKeyboard() {
        activity?.also {
            val inputManager = it.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            it.currentFocus?.let {
                inputManager?.hideSoftInputFromWindow(it.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
            }
        }
    }

    protected fun showSoftwareKeyboardPost(editText: EditText) {
        val etReference: EditText? = editText
        editText.post {
            etReference?.also {
                showSoftwareKeyboard(it)
            }
        }
    }

    protected fun showSoftwareKeyboard(editText: EditText) {
        activity?.also {
            val inputManager = it.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            inputManager?.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
        }
    }
}
