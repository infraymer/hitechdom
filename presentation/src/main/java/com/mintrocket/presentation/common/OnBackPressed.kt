package com.mintrocket.presentation.common

interface OnBackPressed {
    fun onBackPressed()
}