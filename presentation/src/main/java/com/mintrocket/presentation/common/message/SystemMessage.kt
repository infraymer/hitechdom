package com.mintrocket.presentation.common.message

data class SystemMessage(
    val text: String,
    val type: SystemMessageType = SystemMessageType.TOAST
)
