package com.mintrocket.presentation.common

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.widget.TextView
import co.zsmb.materialdrawerkt.builders.drawer
import co.zsmb.materialdrawerkt.draweritems.badgeable.primaryItem
import co.zsmb.materialdrawerkt.draweritems.switchable.switchItem
import com.mikepenz.materialdrawer.Drawer
import com.mintrocket.domain.repository.ISettingsRepository
import com.mintrocket.presentation.extension.attr
import com.mintrocket.presentation.screen.main.MainActivity
import io.reactivex.Observable
import minthouse.mintrocket.com.presentation.R
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.concurrent.TimeUnit


abstract class DrawerFragment : BaseFragment() {

    var drawer: Drawer? = null

    val menuModel: MenuViewModel by viewModel()

    private val settingsRepository: ISettingsRepository by inject()

    override fun initView() {
        super.initView()

        menuModel.drawerItemsLiveData.subscribe {
            drawer = drawer {
                headerDivider = false
                footerDivider = false
                stickyHeaderRes = R.layout.layout_drawer_header
                stickyHeaderShadow = false
                selectedItem = -1
                it.forEach {
                    primaryItem(it.name) {
                        selectable = false
                        iconRes = it.iconResource ?: 0
                        iconColorRes = context!!.attr(R.attr.text_50).resourceId
                        iconTintingEnabled = true
                        onClick { _ ->
                            menuModel.onItemClicked(it)
                            true
                        }
                    }
                }

                switchItem("Темная тема") {
                    selectable = false
                    onClick { _ -> true}
                    checked = settingsRepository.isDarkTheme
                    onSwitchChanged { drawerItem, button, isEnabled ->
                        settingsRepository.isDarkTheme = isEnabled
                    }
                }

                primaryItem("Выйти") {
                    selectable = false
                    onClick { _ ->
                        menuModel.onLogOutClicked()
                        false
                    }
                }
                stickyFooterRes = R.layout.layout_drawer_footer
                stickyFooterDivider = false
                stickyFooterShadow = false
            }
        }

        menuModel.userLiveData.subscribe {
            drawer?.stickyFooter?.apply {
                findViewById<TextView>(R.id.titleTextView).text = "${it.firstName} ${it.lastName}"
            }
        }
    }
}
