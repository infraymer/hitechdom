package com.mintrocket.presentation.common

import android.arch.lifecycle.MutableLiveData
import com.mintrocket.domain.interactor.auth.IAuthInteractor
import com.mintrocket.domain.repository.ISettingsRepository
import com.mintrocket.presentation.controller.UserDataController
import com.mintrocket.presentation.entity.MenuData
import com.mintrocket.presentation.entity.UserData
import com.mintrocket.presentation.navigation.Screens
import com.mintrocket.presentation.screen.global.IErrorHandler
import io.reactivex.Observable
import minthouse.mintrocket.com.presentation.R
import ru.terrakok.cicerone.Router

class MenuViewModel(
    private val router: Router,
    private val interactor: IAuthInteractor,
    private val errorHandler: IErrorHandler,
    private val userController: UserDataController
) : BaseViewModel() {

    companion object {
        const val INDICATIONS = 0
        const val CAMERAS = 1
        const val STOCKS = 2
        const val BIDS = 3
        const val NEWS = 4
        const val CONTACTS = 5
        const val HOME = 6
        const val EXIT = 7
        const val SETTINGS = 8
    }

    val itemsLiveData = MutableLiveData<List<MenuData>>()
    val drawerItemsLiveData = MutableLiveData<List<MenuData>>()
    val userLiveData = MutableLiveData<UserData>()

    init {
        itemsLiveData.value = listOf(
            MenuData(INDICATIONS, "Показания", R.drawable.ic_menu_indications),
            MenuData(CAMERAS, "Камеры", R.drawable.ic_menu_cameras),
            MenuData(STOCKS, "Акции", R.drawable.ic_menu_stocks),
            MenuData(BIDS, "Заявки", R.drawable.ic_menu_bids),
            MenuData(NEWS, "Новости", R.drawable.ic_menu_news),
            MenuData(CONTACTS, "Контакты", R.drawable.ic_menu_contacts)
        )

        drawerItemsLiveData.value = listOf(
            MenuData(HOME, "Главная", R.drawable.ic_drawer_home),
            MenuData(INDICATIONS, "Показания", R.drawable.ic_drawer_indications),
            MenuData(CAMERAS, "Камеры", R.drawable.ic_drawer_cameras),
            MenuData(STOCKS, "Акции", R.drawable.ic_drawer_stocks),
            MenuData(BIDS, "Заявки", R.drawable.ic_drawer_bids),
            MenuData(NEWS, "Новости", R.drawable.ic_drawer_news),
            MenuData(CONTACTS, "Контакты", R.drawable.ic_drawer_contacts),
            MenuData(SETTINGS, "Настройки", R.drawable.ic_drawer_settings)
        )

        userController
            .user
            .subscribe { userLiveData.value = it }
            .addToDisposables()
    }

    fun onItemClicked(item: MenuData) = when (item.id) {
        HOME -> router.newRootScreen(Screens.Home)
        NEWS -> router.newRootChain(Screens.Home, Screens.News)
        else -> Unit
    }

    fun onLogOutClicked() {
        interactor
            .logOut()
            .subscribe({
                router.newRootScreen(Screens.Auth)
            }, {
                errorHandler.proceed(it)
            })
            .addToDisposables()
    }
}