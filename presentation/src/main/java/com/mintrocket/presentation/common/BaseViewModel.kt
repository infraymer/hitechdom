package com.mintrocket.presentation.common

import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign

abstract class BaseViewModel : ViewModel() {
    private val disposable = CompositeDisposable()

    fun Disposable.addToDisposables() {
        disposable.add(this)
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()

    }
}