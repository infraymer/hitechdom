package com.mintrocket.presentation.common.message

enum class SystemMessageType {
    ALERT,
    TOAST
}
