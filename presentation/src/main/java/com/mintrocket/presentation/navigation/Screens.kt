package com.mintrocket.presentation.navigation

import android.support.v4.app.Fragment
import com.mintrocket.presentation.screen.auth.AuthFragment
import com.mintrocket.presentation.screen.home.HomeFragment
import com.mintrocket.presentation.screen.news.ArchiveNewsFragment
import com.mintrocket.presentation.screen.news.NewsDetailFragment
import com.mintrocket.presentation.screen.news.NewsFragment
import com.mintrocket.presentation.screen.notifications.PollFragment
import com.mintrocket.presentation.screen.policy.PolicyFragment
import com.mintrocket.presentation.screen.registration.RegistrationCheckFragment
import com.mintrocket.presentation.screen.registration.RegistrationFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {

    object Auth : SupportAppScreen() {
        override fun getFragment() = AuthFragment()
    }

    object Registration : SupportAppScreen() {
        override fun getFragment() = RegistrationFragment()
    }

    object Policy : SupportAppScreen() {
        override fun getFragment() = PolicyFragment()
    }

    object RegistrationCheck : SupportAppScreen() {
        override fun getFragment() = RegistrationCheckFragment()
    }

    object News : SupportAppScreen() {
        override fun getFragment() = NewsFragment()
    }

    object NewsDetail : SupportAppScreen() {
        override fun getFragment() = NewsDetailFragment()
    }

    object ArchiveNews : SupportAppScreen() {
        override fun getFragment() = ArchiveNewsFragment()
    }

    object Home : SupportAppScreen() {
        override fun getFragment() = HomeFragment()
    }

    object Poll : SupportAppScreen() {
        override fun getFragment() = PollFragment()
    }
}