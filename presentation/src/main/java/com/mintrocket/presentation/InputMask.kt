package com.mintrocket.presentation

object InputMask {

    const val PHONE = "+7 [000] [000]-[00]-[00]"
    const val SMS_CODE = "[0] [0] [0] [0]"
}