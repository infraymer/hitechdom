package com.mintrocket.presentation.entity

data class RegistrationData(
    val firstName: String,
    val secondName: String,
    val lastName: String,
    val city: String,
    val street: String,
    val house: String,
    val housing: String,
    val flat: String
) {


    var phone: String = ""
}