package com.mintrocket.presentation.entity

class NewsData(
    val id: Long,
    val title: String,
    val author: String,
    val content: String,
    val date: Long,
    val image: String
)