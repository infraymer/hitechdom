package com.mintrocket.presentation.entity

class ManagementCompanyData(
    val id: Long,
    val name: String?,
    val email: String?,
    val createdAt: Long?,
    val updatedAt: Long?
)