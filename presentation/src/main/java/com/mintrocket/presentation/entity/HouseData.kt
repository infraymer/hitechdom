package com.mintrocket.presentation.entity

class HouseData(
    val id: Long,
    val street: String?,
    val house: String?,
    val housing: String?,
    val createdAt: Long?,
    val updatedAt: Long?,
    val managementCompany: ManagementCompanyData?
)