package com.mintrocket.presentation.entity

import com.mintrocket.domain.entity.auth.Apartment

class UserData(
    val id: Long?,
    val name: String?,
    val username: String?,
    val firstName: String?,
    val secondName: String?,
    val lastName: String?,
    val email: String?,
    val phone: String?,
    val roleId: Int?,
    val managementCompanyId: Int?,
    val createdAt: Long?,
    val updatedAt: Long?,
    val apartments: List<ApartmentData>?
)