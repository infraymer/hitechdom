package com.mintrocket.presentation.entity

class MenuData(
    val id: Int,
    val name: String,
    val iconResource: Int? = null
)