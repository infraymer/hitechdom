package com.mintrocket.presentation.entity

import com.mintrocket.domain.entity.auth.House

class EntranceData(
    val id: Long,
    val number: String?,
    val storeys: Int?,
    val createdAt: Long?,
    val updatedAt: Long?,
    val house: HouseData?
)