package com.mintrocket.presentation.entity

class ApartmentData(
    val id: Long,
    val number: String?,
    val createdAt: Long?,
    val entrance: EntranceData?
) {

    val fullAddress: String
        get() {
            val house = entrance?.house
            val street = house?.street
            val numHouse = house?.house
            val housing = house?.housing
            val flat = number
            return "ул. $street, д. $numHouse, " +
                (if (housing != null && housing.isNotEmpty()) "корп. $housing, " else "") +
                "кв. $flat"
        }
}