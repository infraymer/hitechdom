package com.mintrocket.presentation.converter

import com.mintrocket.domain.entity.management_company.ManagementCompany
import com.mintrocket.presentation.entity.ManagementCompanyData

fun ManagementCompany.toData() = ManagementCompanyData(
    id, name, email, createdAt, updatedAt
)

fun ManagementCompanyData.toDomain() = ManagementCompany(
    id, name, email, createdAt, updatedAt
)