package com.mintrocket.presentation.converter

import com.mintrocket.domain.entity.news.News
import com.mintrocket.presentation.entity.NewsData

fun News.toData() = NewsData(id, name, author, content, publishedAt, "")