package com.mintrocket.presentation.converter

import com.mintrocket.domain.entity.Address
import com.mintrocket.domain.entity.auth.Registration
import com.mintrocket.presentation.entity.RegistrationData

fun RegistrationData.toDomain() = Registration(
    firstName, secondName, lastName, city, street, house, housing, flat
)

fun Registration.toData() = RegistrationData(
    firstName, secondName, lastName, city, street, house, housing, flat
)

fun RegistrationData.toAddress() = Address(
    city, street, house, housing, flat
)