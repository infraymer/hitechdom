package com.mintrocket.presentation.converter

import com.mintrocket.domain.entity.auth.Apartment
import com.mintrocket.domain.entity.auth.Entrance
import com.mintrocket.domain.entity.auth.House
import com.mintrocket.domain.entity.auth.User
import com.mintrocket.presentation.entity.ApartmentData
import com.mintrocket.presentation.entity.EntranceData
import com.mintrocket.presentation.entity.HouseData
import com.mintrocket.presentation.entity.UserData

fun User.toData() = UserData(
    id, name, username,  firstName, secondName, lastName, email, phone, roleId, managementCompanyId, createdAt, updatedAt, apartments?.map { it.toData() }
)

fun Apartment.toData() = ApartmentData(
    id, number, createdAt, entrance?.toData()
)

fun Entrance.toData() = EntranceData(
    id, number, storeys, createdAt, updatedAt, house?.toData()
)

fun House.toData() = HouseData(
    id, street, house, housing, createdAt, updatedAt, managementCompany?.toData()
)