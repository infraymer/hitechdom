package com.mintrocket.presentation.extension

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.support.annotation.AttrRes
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import minthouse.mintrocket.com.presentation.R
import java.io.File

fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View =
    LayoutInflater.from(this.context).inflate(layoutId, this, attachToRoot)

fun View.getColor(color: Int) = ContextCompat.getColor(context, color)

fun View.visible(show: Boolean = true, gone: Boolean = true) {
    visibility = if (show) View.VISIBLE else if (gone) View.GONE else View.INVISIBLE
}

fun ImageView.loadImage(path: String, placeHolder: Drawable? = null) = Glide.with(this)
    .load(path)
    .apply(RequestOptions().placeholder(placeHolder).override(Target.SIZE_ORIGINAL))
    .transition(DrawableTransitionOptions.withCrossFade())
    .into(this)


fun ImageView.loadImage(file: File, placeHolder: Drawable? = null) = Glide.with(this)
    .load(file)
    .apply(RequestOptions().placeholder(placeHolder).override(Target.SIZE_ORIGINAL))
    .transition(DrawableTransitionOptions.withCrossFade())
    .into(this)

fun TextView.addOnTextChanged(listener: (s: CharSequence, start: Int, before: Int, count: Int, view: TextView) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {

        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            listener(s, start, before, count, this@addOnTextChanged)
        }
    })
}

fun TextView.setHtmlText(html: String) {
    text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT)
    } else {
        Html.fromHtml(html)
    }
}

fun Fragment.dip(int: Int) = (int * resources.displayMetrics.density).toInt()
fun Fragment.dip(float: Float) = (float * resources.displayMetrics.density).toFloat()
fun Context.dip(int: Int) = (int * resources.displayMetrics.density).toInt()
fun Context.dip(float: Float) = (float * resources.displayMetrics.density).toFloat()


val matchParent: Int = android.view.ViewGroup.LayoutParams.MATCH_PARENT
val wrapContent: Int = android.view.ViewGroup.LayoutParams.WRAP_CONTENT

fun Context.attr(@AttrRes idRes: Int): TypedValue {
    val typedValue = TypedValue()
    val theme = theme
    theme?.resolveAttribute(idRes, typedValue, true)
    return typedValue
}
