package com.mintrocket.presentation.extension

import android.app.Activity
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.support.v7.app.AppCompatActivity

fun <T> MutableLiveData<T>.subscribe(activity: AppCompatActivity, action: (T) -> Unit) {
    observe(activity, Observer { it?.let { action(it) } })
}