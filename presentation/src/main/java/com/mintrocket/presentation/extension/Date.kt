package com.mintrocket.presentation.extension

import com.mintrocket.presentation.extension.DateHelper.DF_SERVER_STRING
import com.mintrocket.presentation.extension.DateHelper.LOCALE
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

object DateHelper {
    const val DF_SIMPLE_STRING = "dd.MM.yyyy"
    const val DF_SERVER_STRING = "YYYY-MM-dd HH:mm:ss"

    val LOCALE = Locale("ru", "RU")

    @JvmField
    val DF_SIMPLE_FORMAT = object : ThreadLocal<DateFormat>() {
        override fun initialValue(): DateFormat = SimpleDateFormat(DF_SIMPLE_STRING, Locale.getDefault())
    }
}

fun dateNow(): String = Date().asString()

fun timestamp(): Long = System.currentTimeMillis()

fun Date.asString(format: DateFormat): String = format.format(this)

fun Date.asString(format: String): String = asString(SimpleDateFormat(format, LOCALE))

fun Date.asString(): String = DateHelper.DF_SIMPLE_FORMAT.get().format(this)

fun Long.asDate(): Date = Date(this * 1000)

fun String.asTime() = SimpleDateFormat(DF_SERVER_STRING).parse(this).time

fun Long.asDateString(): String = asDate().asString()

fun Long.asDateString(format: String): String = asDate().asString(format)

fun dateTimestamp(): Long = (Date().time / 86400000) * 86400

fun String.changeFormat(format: String): String {
    val df = DateFormat.getInstance()
    val date = df.parse(this)
    return date.asString(format)
}



fun currentMonth(): String {
    val cal = Calendar.getInstance()
    val monthDate = SimpleDateFormat("LLLL", LOCALE)
    return monthDate.format(cal.time).toLowerCase()
}

fun Date.isSameDay(date: Date): Boolean {
    val cal1: Calendar = Calendar.getInstance()
    cal1.time = this
    val cal2: Calendar = Calendar.getInstance()
    cal2.time = date
    return cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
        cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
        cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR)
}

fun Date.isYesterday(date: Date): Boolean {
    val cal1: Calendar = Calendar.getInstance()
    cal1.time = this
    val cal2: Calendar = Calendar.getInstance()
    cal2.time = date
    cal2.add(Calendar.DAY_OF_YEAR, -1)
    return cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
        cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
        cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR)
}

fun Long.asYearString(): String = Calendar.getInstance().apply {
    timeInMillis = this@asYearString * 1000
}.get(Calendar.YEAR).toString()

fun Long.asMonthString(): String {
    val calendar = Calendar.getInstance().apply {
        timeInMillis = this@asMonthString * 1000
    }

    val month = calendar.get(Calendar.MONTH).monthToShortString()
    val year = calendar.get(Calendar.YEAR)

    return "$month. $year г."
}

fun Long.asDayString(short: Boolean = false): String {
    val calendar = Calendar.getInstance().apply {
        timeInMillis = this@asDayString * 1000
    }

    val day = calendar.get(Calendar.DAY_OF_MONTH)
    val month =
        if (short) calendar.get(Calendar.MONTH).monthToShortString()
        else calendar.get(Calendar.MONTH).monthToString()
    val year = calendar.get(Calendar.YEAR)

    return if (short) "$day $month. $year г." else "$day $month $year"
}

fun Int.monthToShortString() = when (this) {
    0 -> "янв"
    1 -> "Фев"
    2 -> "мар"
    3 -> "апр"
    4 -> "май"
    5 -> "июн"
    6 -> "июл"
    7 -> "авг"
    8 -> "сен"
    9 -> "окт"
    10 -> "ноя"
    else -> "дек"
}

fun Int.monthToString() = when (this) {
    0 -> "января"
    1 -> "февраля"
    2 -> "марта"
    3 -> "апреля"
    4 -> "мая"
    5 -> "июня"
    6 -> "июля"
    7 -> "августа"
    8 -> "сентября"
    9 -> "октября"
    10 -> "ноября"
    else -> "декабря"
}

fun Long.daysInMonth() = Calendar.getInstance().apply {
    timeInMillis = this@daysInMonth
}.getActualMaximum(Calendar.DAY_OF_MONTH)

val Long.hour: Int
    get() = Calendar.getInstance().apply {
        timeInMillis = this@hour
    }.get(Calendar.HOUR)

val Long.day: Int
    get() = Calendar.getInstance().apply {
        timeInMillis = this@day
    }.get(Calendar.DAY_OF_MONTH)

val Long.month: Int
    get() = Calendar.getInstance().apply {
        timeInMillis = this@month
    }.get(Calendar.MONTH)

fun Long.plusHour(hour: Int = 1) = Calendar.getInstance().apply {
    timeInMillis = this@plusHour
    add(Calendar.HOUR_OF_DAY, hour)
}.timeInMillis

fun Long.minusHour(hour: Int = -1) = Calendar.getInstance().apply {
    timeInMillis = this@minusHour
    add(Calendar.HOUR_OF_DAY, hour)
}.timeInMillis

fun Long.plusDay(day: Int = 1) = Calendar.getInstance().apply {
    timeInMillis = this@plusDay
    add(Calendar.DAY_OF_MONTH, day)
}.timeInMillis

fun Long.minusDay(day: Int = -1) = Calendar.getInstance().apply {
    timeInMillis = this@minusDay
    add(Calendar.DAY_OF_MONTH, day)
}.timeInMillis

fun Long.plusMonth(month: Int = 1) = Calendar.getInstance().apply {
    timeInMillis = this@plusMonth
    add(Calendar.MONTH, month)
}.timeInMillis

fun Long.minusMonth(month: Int = -1) = Calendar.getInstance().apply {
    timeInMillis = this@minusMonth
    add(Calendar.MONTH, month)
}.timeInMillis

fun Long.plusYear(year: Int = 1) = Calendar.getInstance().apply {
    timeInMillis = this@plusYear
    add(Calendar.YEAR, year)
}.timeInMillis

fun Long.minusYear(year: Int = -1) = Calendar.getInstance().apply {
    timeInMillis = this@minusYear
    add(Calendar.YEAR, year)
}.timeInMillis
