package com.mintrocket.hitechhome

import android.app.Application
import com.crashlytics.android.Crashlytics
import com.mintrocket.data.di.dataModule
import com.mintrocket.domain.domainModule
import com.mintrocket.presentation.presentationModule
import io.fabric.sdk.android.Fabric
import org.koin.android.ext.android.startKoin
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG.not()) {
            Fabric.with(this, Crashlytics())
        }

        Timber.plant(Timber.DebugTree())
        startKoin(this, listOf(domainModule, presentationModule, dataModule))
    }
}